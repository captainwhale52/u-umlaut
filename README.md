# Ü (U-umlaut) Platform

## Overview of the app
Ü is a mobile application that we (Karl, Chloe, Fei, Jing, Celine, Hank, and Eason) designed for couples who are in long-distance relationship to create their memory gallery by recording their treasure moments with Ü because when they are apart, it is difficult for them to experience life together. Our idea is to be as a platform for them to make them feel connected with each other even when they are apart from each other due to various reasons such as studying abroad, going on a business trip and etc. Through Ü,  they are going to create their own personalized storybooks, which they are able to share the missing moments together and provide a feeling of presence and connection.

## About this repository
This repository contains both the server-side and client-side (app) codes for developing, testing, deploying the platform to any clouding computing server (e.g. Google Cloud, Amazon Web Service). The server-side is developed in Django, and the client-side is developed in React. It uses a Docker container for handling automated deployment process.
This repository is managed by Karl Kim. If you want an access for the latest Bitbucket repository `https://captainwhale52@bitbucket.org/captainwhale52/u-umlaut.git`, please send an email to `captainwhale52@gmail.com`.

## Requirement
- Python >= 3.6.2
- Node >= 8.8.1
- Virtualenv >= 15.1.0
- Docker >= 17.09.0-ce
- Docker Compose >= 1.16.1

## Installation
- Open a new terminal.
- `cd {project_root}/web`
- `pip install -r requirements.txt` to install python dependent libraries.
- `cd {project_root}/web/frontend`
- `npm install` to install node dependent libraries.

## Database Setup
- Open a new terminal.
- `cd {project_root}/web`
- `python manage.py migrate` to create a DB file (db.sqlite3) & database tables.

## Development
- Open a new terminal.
- `python manage.py runserver 0.0.0.0:8000` to run a local server with development mode.
- Open a new terminal (or a new tab).
- `cd {project_root}/web/frontend`
- `npm run beta` to run a Webpack packager with the watch mode (it will recompile the code every time you made a change).

## Deployment
- Open a new terminal.
- `npm run prod` to run a Webpack packager with the production mode (it will minify JavaScript codes and omit all comments and console logs).
- `cd {project_root}`
- `sudo docker-compose up -d` to run a docker compose to create docker container with the current source codes (-d option will make the container running in a background).

## Re-deployment
The deployment pipeline is not fully automated. So if you change the frontend code, you need to remove the current docker container and re-build it.

- `sudo docker-compose down` to shutdown a current running docker container.
- `docker images` to search all docker containers in your system.
- Find docker container name like `uumlaut_web`.
- Copy the image id of the container.
- `docker rmi {container_image_id}` or `docker rmi uumlaut_web` to remove the current docker image.
- `sudo docker-compose up -d` to build and run a new docker container.
