const fs = require('fs');
const path = require('path');

const yaml = require('js-yaml');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const BundleTracker = require('webpack-bundle-tracker');



const root = path.resolve(__dirname);

const static = path.join(root, './../static');
const scripts = path.join(static, 'scripts');

const src = path.join(root, './src');
const styles = path.join(src, './styles');
const apps = path.join(src, './apps');
const modules = path.join(root, './node_modules');



//const libraries = path.join(src, './libraries');
//const statics = path.join(src, './statics');
//const templates = path.join(src, './templates');

module.exports = function (stage) {
    process.env.NODE_ENV = stage;
    process.env.BABEL_ENV = stage;

    switch (stage) {
    case 'development': {
        break;
    }
    default: {
        break;
    }
    }

    const defines = {};
    defines['process.env'] = {
        NODE_ENV: JSON.stringify(stage)
    };

    const entry = {};
    if (stage === 'development') {
        const args = JSON.parse(process.env.npm_config_argv)['cooked'];
        for (let i = 2; i < args.length; i += 2) {
            if (JSON.parse(process.env.npm_config_argv)['cooked'][i] === '--app') {
                const view = JSON.parse(process.env.npm_config_argv)['cooked'][i + 1];
                entry[view] = path.join(apps, `./${view}/index.js`);
            }
        }
    } else {
        // NEED TO ADD EACH APP UNDER HERE!!!
        entry['splash'] = path.join(apps, './splash/index.js');
        entry['home'] = path.join(apps, './home/index.js');
        entry['photo'] = path.join(apps, './photo/index.js');
        entry['composite'] = path.join(apps, './composite/index.js');
        entry['dialogue'] = path.join(apps, './dialogue/index.js');
        entry['channels'] = path.join(apps, './channels/index.js');
        entry['channel'] = path.join(apps, './channel/index.js');
        entry['story'] = path.join(apps, './story/index.js');
        entry['user'] = path.join(apps, './user/index.js');
        entry['signup'] = path.join(apps, './signup/index.js');
    }

    const plugins = [
        new webpack.DefinePlugin(defines),
    ];

    if (stage === 'development') {
        plugins.push(
            new HtmlWebpackPlugin({
                template: path.join(src, 'index.ejs'),
            }),
            new webpack.HotModuleReplacementPlugin()
        );
    } else if (stage === 'beta') {
        plugins.push(
            new BundleTracker({filename: './../static/webpack-stats.json'}),
            new CleanWebpackPlugin(scripts, { root: static }),
        );
    } else {
        plugins.push(
            new BundleTracker({filename: './../static/webpack-stats.json'}),
            new CleanWebpackPlugin(scripts, { root: static }),
            new ExtractTextPlugin('[name].[hash].css'),
        );
    }

    const config ={
        devtool: stage === 'development' || stage === 'beta' ? 'source-map' : 'eval',
        context: src,
        entry,
        output: {
            path: scripts,
            filename: '[name].[hash].js',
            publicPath: stage === 'development' ? '/' : '/static/'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [
                        'babel-loader'
                    ]
                },
                {
                    test: /\.(css|sass|scss)$/,
                    include: styles,
                    use: stage === 'production' ? ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            { loader: 'css-loader', query: { modules: true, sourceMaps: false } },
                            { loader: 'postcss-loader' },
                            { loader: 'sass-loader' }
                        ]
                    }) : [
                        'style-loader',
                        'css-loader?modules&localIdentName=[local]',
                        'sass-loader'
                    ]
                }
            ]
        },
        plugins: plugins,
        devServer: {
            contentBase: static,
            historyApiFallback: true,
            port: 3000,
            compress: false,
            inline: true,
            hot: true,
            stats: {
                assets: true,
                children: false,
                chunks: false,
                hash: false,
                modules: false,
                publicPath: false,
                timings: true,
                version: false,
                warnings: true,
                colors: {
                    green: '\u001b[32m',
                }
            }
        },
        node: {
            fs: 'empty',
            net: 'empty',
            tls: 'empty'
        }
    };

    return config;
};