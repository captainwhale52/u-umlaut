const inquirer = require('inquirer');
const spawn = require('child_process').spawn;

const questions = [
    {
        type: 'rawlist',
        name: 'app',
        message: 'Which app do you want to compile?',
        // NEED TO ADD EACH MODULE!!!
        choices: [
            'splash',
            'home',
            'photo',
            'composite',
            'dialogue',
            'channels',
            'channel',
            'story',
            'user',
            'signup',
            'none'
        ]
    }
];

inquirer.prompt(questions).then(function (answers) {
    if (answers.app === 'none') {
        return;
    }
    spawn('npm', ['run', 'local', `--app=${answers.app}`], { stdio: 'inherit' });
});