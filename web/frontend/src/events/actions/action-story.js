import Immutable from 'seamless-immutable';
import api from './api';
import store from '../../events/stores/store-index';


function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

const uploadStory = image => (dispatch) => {
    dispatch({
        type: 'POST_STORY_PENDING',
    });
    const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };

    const blob = dataURItoBlob(dataURL);
    const data = new FormData(document.forms[0]);
    data.append("image", blob);


    // const data = new FormData();
    // data.append('image', {
    //   uri: image,
    //   type: 'image/png', // or photo.type
    //   name: 'photo.png'
    // });
    //
    // console.log(image);
    // console.log(data);

    return api.post('api/stories/images/', data, config).then((success) => {
        dispatch({
            type: 'POST_STORY_FULFILLED',
            payload: success.data
        });
        console.log(success);
        return success.data;
    }).catch((error) => {
        dispatch({
            type: 'POST_STORY_REJECTED',
        });
        console.log(error);
        return error;
    });

    // return api.get('api/images/').then((success) => {
    //     // dispatch({
    //     //     type: 'TEXT_INPUT_FULFILLED',
    //     //     payload: success.data
    //     // });
    //     console.log(success);
    //     return success;
    // }).catch((error) => {
    //     // dispatch({
    //     //     type: 'TEXT_INPUT_REJECTED',
    //     // });
    //     console.log(error);
    //     return error;
    // });
};




const fetchStories = () => (dispatch) => {
    dispatch({
        type: 'FETCH_STORIES_PENDING',
    });

    return api.get('api/stories/').then((success) => {
        dispatch({
            type: 'FETCH_STORIES_FULFILLED',
            payload: {
                stories: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_STORIES_REJECTED',
            payload: error,
        });
    });
};

const fetchStory = (story_id) => (dispatch) => {
    dispatch({
        type: 'FETCH_STORY_PENDING',
    });

    return api.get(`api/stories/${story_id}/`).then((success) => {
        dispatch({
            type: 'FETCH_STORY_FULFILLED',
            payload: {
                story: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_STORY_REJECTED',
            payload: error,
        });
    });
};



const selectPanel = (index) => (dispatch) => {
    dispatch({
        type: 'SELECT_PANEL_FULFILLED',
        payload: {
            index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const setPanelPhoto = (image) => (dispatch) => {
    dispatch({
        type: 'SET_PANEL_PHOTO_FULFILLED',
        payload: {
            image
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const setPanelScale = (scale) => (dispatch) => {
    dispatch({
        type: 'SET_PANEL_SCALE_FULFILLED',
        payload: {
            scale
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const movePanelPhoto = (x, y) => (dispatch) => {
    dispatch({
        type: 'MOVE_PANEL_PHOTO_FULFILLED',
        payload: {
            x, y
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const updateStory = (dataURL) => (dispatch) => {
    dispatch({
        type: 'UPDATE_STORY_PENDING',
    });
    const story = Immutable.asMutable(store.getState().story.story, {deep: true});


    const texts = story.texts.filter((text, index) => {
        return text !== null;
    });

    const blob = dataURItoBlob(dataURL);
    const data = new FormData(document.forms[0]);
    data.append('thumbnail', blob);
    data.append('title', story.title);
    data.append('photos', JSON.stringify(story.photos));
    data.append('texts', JSON.stringify(texts));
    data.append('icons', JSON.stringify(story.icons));

    console.log(data);

    return api.put(`api/stories/${story.pk}/update/`, data).then((success) => {
        console.log(success);
        dispatch({
            type: 'UPDATE_STORY_FULFILLED',
            payload: {
                channel: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'UPDATE_STORY_REJECTED',
            payload: error,
        });
    });
};

const sendStory = (story_id) => (dispatch) => {
    dispatch({
        type: 'SEND_STORY_PENDING',
    });

    return api.put(`api/stories/${story_id}/send/`).then((success) => {
        console.log(success);
        dispatch({
            type: 'SEND_STORY_FULFILLED',
            payload: {
                story: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'SEND_STORY_REJECTED',
            payload: error,
        });
    });
};

const createStory = (title, channel_id) => (dispatch) => {
    const titles = [
        'Hold Me in the Rain',
        'Keeping Memories',
        'Trapped',
        'Our Love',
        'Toxic Lips',
        "Angel's Can't Die",
        "Close My Eyes",
        "Into My Heart"

    ];
    if (title === '') {
        title = titles[parseInt(Math.random() * titles.length)];
    }
    dispatch({
        type: 'CREATE_STORY_PENDING',
    });

    return api.post(`api/stories/create/`, {
        title,
        channel: channel_id
    }).then((success) => {
        console.log(success);
        dispatch({
            type: 'CREATE_STORY_FULFILLED',
            payload: {
                story: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'CREATE_STORY_REJECTED',
            payload: error,
        });
    });
};

const addText = (text) => (dispatch) => {
    dispatch({
        type: 'ADD_TEXT_FULFILLED',
        payload: {
            text
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const removeText = (index) => (dispatch) => {
    dispatch({
        type: 'REMOVE_TEXT_FULFILLED',
        payload: {
            index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const selectText = (index) => (dispatch) => {
    dispatch({
        type: 'SELECT_TEXT_FULFILLED',
        payload: {
            index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const movePanelText = (x, y) => (dispatch) => {
    dispatch({
        type: 'MOVE_PANEL_TEXT_FULFILLED',
        payload: {
            x, y
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const changePanelText = (text) => (dispatch) => {
    dispatch({
        type: 'CHANGE_PANEL_TEXT_FULFILLED',
        payload: {
            text
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const changePanelTextColor = (color) => (dispatch) => {
    dispatch({
        type: 'CHANGE_PANEL_TEXT_COLOR_FULFILLED',
        payload: {
            color
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const changeTextSize = (font_size) => (dispatch) => {
    dispatch({
        type: 'CHANGE_TEXT_SIZE_FULFILLED',
        payload: {
            font_size
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const changeTitle = (title) => (dispatch) => {
    dispatch({
        type: 'CHANGE_TITLE_FULFILLED',
        payload: {
            title
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const selectControllerMode = (mode) => (dispatch) => {
    dispatch({
        type: 'SELECT_CONTROLLER_MODE',
        payload: {
            mode
        }
    });
    return Promise.resolve(
    ).then(success => success);
};


const selectPhotoMode = (mode) => (dispatch) => {
    dispatch({
        type: 'SELECT_PHOTO_MODE',
        payload: {
            mode
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const selectTextMode = (mode) => (dispatch) => {
    dispatch({
        type: 'SELECT_TEXT_MODE',
        payload: {
            mode
        }
    });
    return Promise.resolve(
    ).then(success => success);
};



const deleteStory = (story_id) => (dispatch) => {
    dispatch({
        type: 'DELETE_STORY_PENDING',
    });

    return api.delete(`api/stories/${story_id}/delete/`).then((success) => {
        console.log(success);
        dispatch({
            type: 'DELETE_STORY_FULFILLED',
            payload: {
                story: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'DELETE_STORY_REJECTED',
            payload: error,
        });
    });
};

const selectStory = (story_pk) => (dispatch) => {
    dispatch({
        type: 'SELECT_STORY_FULFILLED',
        payload: {
            pk: story_pk
        }
    });
    return Promise.resolve(
    ).then(success => success);
};


export {
    uploadStory,
    fetchStories,
    fetchStory,
    updateStory,

    setPanelPhoto,
    selectPanel,
    setPanelScale,
    movePanelPhoto,
    sendStory,
    createStory,
    addText,
    selectText,
    movePanelText,
    changePanelText,
    changePanelTextColor,
    changeTextSize,

    selectControllerMode,
    selectPhotoMode,
    selectTextMode,
    removeText,
    changeTitle,

    deleteStory,
    selectStory
};
