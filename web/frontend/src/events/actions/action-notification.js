import api from './api';

const addNotification = (item) => (dispatch) => {
    dispatch({
        type: 'ADD_NOTIFICATION_FULFILLED',
        payload: {
            item
        }
    });

    setTimeout(() => {
        dispatch({
            type: 'REMOVE_NOTIFICATION_FULFILLED',
            payload: {
                item
            }
        });
    }, 10000);

    return Promise.resolve(
    ).then(success => success);
};

const removeNotification = (item) => (dispatch) => {
    dispatch({
        type: 'REMOVE_NOTIFICATION_FULFILLED',
    });
    return Promise.resolve(
    ).then(success => success);
};

export {
    addNotification,
    removeNotification
};