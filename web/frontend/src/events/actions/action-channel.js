import api from './api';

const fetchChannels = () => (dispatch) => {
    dispatch({
        type: 'FETCH_CHANNELS_PENDING',
    });

    return api.get('api/channels/').then((success) => {
        dispatch({
            type: 'FETCH_CHANNELS_FULFILLED',
            payload: {
                channels: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_CHANNELS_REJECTED',
            payload: error,
        });
    });
};

const updateChannel = (channel_id, channel_status) => (dispatch) => {
    dispatch({
        type: 'UPDATE_CHANNEL_PENDING',
    });

    return api.put(`api/channels/${channel_id}/update/`, {
        status: channel_status
    }).then((success) => {
        console.log(success);
        dispatch({
            type: 'UPDATE_CHANNEL_FULFILLED',
            payload: {
                channel: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'UPDATE_CHANNEL_REJECTED',
            payload: error,
        });
    });
};

const createChannel = (recipient_id, channel_type) => (dispatch) => {
    dispatch({
        type: 'CREATE_CHANNEL_PENDING',
    });

    return api.post(`api/channels/create/`, {
        recipient: recipient_id,
        type: channel_type
    }).then((success) => {
        console.log(success);
        dispatch({
            type: 'CREATE_CHANNEL_FULFILLED',
            payload: {
                channel: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'CREATE_CHANNEL_REJECTED',
            payload: error,
        });
    });
};

const fetchChannel = (channel_id) => (dispatch) => {
    dispatch({
        type: 'FETCH_CHANNEL_PENDING',
    });

    return api.get(`api/channels/${channel_id}/`).then((success) => {
        console.log(success);
        dispatch({
            type: 'FETCH_CHANNEL_FULFILLED',
            payload: {
                channel: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_CHANNEL_REJECTED',
            payload: error,
        });
    });
};

export {
    fetchChannels,
    updateChannel,
    createChannel,
    fetchChannel
};