const setOverlayActivated = activated => (dispatch) => {
    dispatch({
        type: 'SET_OVERLAY_ACTIVATED',
        payload: {
            activated
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const setOverlayMode = mode => (dispatch) => {
    dispatch({
        type: 'SET_OVERLAY_MODE',
        payload: {
            mode
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export {
    setOverlayActivated,
    setOverlayMode
};