

const setCompositeZoomInByIndex = (index) => (dispatch) => {
    dispatch({
        type: 'SET_COMPOSITE_ZOOM_IN_BY_INDEX',
        payload: {
            index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

const setCompositeZoomOutByIndex = (index) => (dispatch) => {
    dispatch({
        type: 'SET_COMPOSITE_ZOOM_OUT_BY_INDEX',
        payload: {
            index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};








export {


    setCompositeZoomInByIndex,
    setCompositeZoomOutByIndex,
    // setTextTextByIndex,
    // setTextColorByIndex,

};