import api from './api';

import cookie from 'js-cookie';

const fetchAuth = () => (dispatch) => {
    if (cookie.get('authentication') === undefined) {
        dispatch({
            type: 'FETCH_AUTH_REJECTED',
            payload: {
                error: 'No credential exists.'
            }
        });
        return;
    }
    dispatch({
        type: 'FETCH_AUTH_PENDING',
    });

    return api.post('api/users/verify/', {
        token: cookie.get('authentication')
    }).then((success) => {
        dispatch({
            type: 'FETCH_AUTH_FULFILLED',
            payload: {
                user: success.data.user
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_AUTH_REJECTED',
            payload: error,
        });
    });
};

const postLogin = (username, password) => (dispatch) => {
    dispatch({
        type: 'POST_LOGIN_PENDING',
    });

    return api.post('api/users/login/', {
        username, password
    }).then((success) => {
        dispatch({
            type: 'POST_LOGIN_FULFILLED',
            payload: {
                user: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'POST_LOGIN_REJECTED',
            payload: error,
        });
    });
};

const fetchLogout = () => (dispatch) => {
    cookie.remove('authentication');
    dispatch({
        type: 'FETCH_LOGOUT_FULFILLED',
    });

    // return api.post('api/users/logout/').then((success) => {
    //     console.log(success);
    //     dispatch({
    //         type: 'FETCH_LOGOUT_FULFILLED',
    //         payload: {
    //             user: success.data
    //         }
    //     });
    // }).catch((error) => {
    //     dispatch({
    //         type: 'FETCH_LOGOUT_REJECTED',
    //         payload: error,
    //     });
    // });
};

const postRegister = (username, email, first_name, last_name, password) => (dispatch) => {
    dispatch({
        type: 'POST_REGISTER_PENDING',
    });

    return api.post('api/users/register/', {
        username, email, first_name, last_name, password
    }).then((success) => {
        dispatch({
            type: 'POST_REGISTER_FULFILLED',
            payload: {
                user: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'POST_REGISTER_REJECTED',
            payload: error,
        });
    });
};


const searchUser = (email) => (dispatch) => {
    if (cookie.get('authentication') === undefined) {
        dispatch({
            type: 'SEARCH_USER_REJECTED',
            payload: {
                error: 'No credential exists.'
            }
        });
        return;
    }
    dispatch({
        type: 'SEARCH_USER_PENDING',
    });

    return api.get(`api/users/search/?email=${email}`)
        .then((success) => {
            console.log(success);
            dispatch({
                type: 'SEARCH_USER_FULFILLED',
                payload: {
                    user: success.data
                }
            });
        }).catch((error) => {
            dispatch({
                type: 'SEARCH_USER_REJECTED',
                payload: error,
            });
        });
};

export {
    fetchAuth,
    postLogin,
    fetchLogout,
    postRegister,
    searchUser
};