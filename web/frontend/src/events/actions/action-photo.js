import api from './api';

const fetchPhotos = () => (dispatch) => {
    dispatch({
        type: 'FETCH_PHOTOS_PENDING',
    });

    return api.get('api/photos/').then((success) => {
        dispatch({
            type: 'FETCH_PHOTOS_FULFILLED',
            payload: {
                photos: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'FETCH_PHOTOS_REJECTED',
            payload: error,
        });
    });
};


const postPhoto = (image) => (dispatch) => {
    console.log(image);
    dispatch({
        type: 'POST_PHOTO_PENDING',
    });

    const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
    const data = new FormData();
    data.append('image', image);

    return api.post('api/photos/create/', data, config).then((success) => {
        console.log(success);
        dispatch({
            type: 'POST_PHOTO_FULFILLED',
            payload: {
                photos: success.data
            }
        });
    }).catch((error) => {
        dispatch({
            type: 'POST_PHOTO_REJECTED',
            payload: error,
        });
    });
};


export {
    fetchPhotos,
    postPhoto
};

