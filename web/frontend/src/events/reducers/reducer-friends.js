import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    friends: [
        {
            name: 'Eason',
            email: 'eason@uumlaut.com'
        },
        {
            name: 'Hank',
            email: 'hank@uumlaut.com'
        }
    ]
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        default: {
            return state;
        }
    }
}
