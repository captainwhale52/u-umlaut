import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    activated: false,
    mode: GLOBALS.OVERLAY.MODE.NONE,
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'SET_OVERLAY_ACTIVATED': {
            let mode = state.mode;
            if (!action.payload.activated) {
                mode = GLOBALS.OVERLAY.MODE.NONE;
            }
            return state.merge({ activated: action.payload.activated, mode: mode });
        }
        case 'SET_OVERLAY_MODE': {
            return state.merge({ mode: action.payload.mode });
        }
        default: {
            return state;
        }
    }
}