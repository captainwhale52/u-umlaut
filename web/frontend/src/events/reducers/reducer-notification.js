import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    notifications: [],
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'ADD_NOTIFICATION_FULFILLED': {
            const notifications = Immutable.asMutable(state.notifications, {deep: true});
            notifications.push(action.payload.item);
            return state.merge({ notifications });
        }

        case 'REMOVE_NOTIFICATION_FULFILLED': {
            const notifications = Immutable.asMutable(state.notifications, {deep: true});
            notifications.shift();
            return state.merge({ notifications });
        }

        default: {
            return state;
        }
    }
}