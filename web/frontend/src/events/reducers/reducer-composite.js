import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    editing_panel_index: null,
    editing_text_index: null,
    photos: [
        null, null, null, null, null
    ],
    scales: [
        1, 1, 1, 1, 1
    ],
    texts: [
        // {
        //     text: 'Test Text',
        //     position: {
        //         x: 16, y: 16
        //     },
        //     color: '#000000'
        // }
    ]
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'SET_COMPOSITE_SCALE_BY_INDEX': {
            const scales = Immutable.asMutable(state.scales);
            scales[action.payload.index] = action.payload.scale;
            return state.merge({ scales });
        }
        case 'SET_COMPOSITE_ZOOM_IN_BY_INDEX': {
            const scales = Immutable.asMutable(state.scales);
            scales[action.payload.index] *= 1.25;
            return state.merge({ scales });
        }
        case 'SET_COMPOSITE_ZOOM_OUT_BY_INDEX': {
            const scales = Immutable.asMutable(state.scales);
            scales[action.payload.index] /= 1.25;
            return state.merge({ scales });
        }



        case 'SET_TEXT_COLOR_BY_INDEX': {
            const texts = Immutable.asMutable(state.texts, {deep: true});
            texts[action.payload.index].color = action.payload.color;
            return state.merge({ texts });
        }

        default: {
            return state;
        }
    }
}