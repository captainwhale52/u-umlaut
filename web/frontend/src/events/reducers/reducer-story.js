import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    story: null,
    selected_panel_index: null,
    selected_text_index: null,
    stories: [],
    controller_mode: GLOBALS.CONTROLLER.MODE.NONE,
    photo_mode: GLOBALS.CONTROLLER.PHOTO.NONE,
    text_mode: GLOBALS.CONTROLLER.TEXT.NONE,
    dirty: false,
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'UPDATE_STORY_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'UPDATE_STORY_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'UPDATE_STORY_FULFILLED': {
            return state.merge({ fetching: false, fetched: true, error: null, dirty: false });
        }

        case 'FETCH_STORIES_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_STORIES_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_STORIES_FULFILLED': {
            return state.merge({ fetching: false, fetched: true, error: null, stories: action.payload.stories });
        }

        case 'FETCH_STORY_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_STORY_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_STORY_FULFILLED': {
            const story = action.payload.story;
            story.panels = JSON.parse(story.panels);
            story.photos = JSON.parse(story.photos);
            story.properties = JSON.parse(story.properties);
            story.texts = JSON.parse(story.texts);
            story.icons = JSON.parse(story.icons);
            return state.merge({ fetching: false, fetched: true, error: null, story });
        }

        case 'SELECT_PANEL_FULFILLED': {
            if (action.payload.index === null) {
                return state.merge({
                    selected_panel_index: action.payload.index,
                    photo_mode: GLOBALS.CONTROLLER.PHOTO.NONE,
                    controller_mode: GLOBALS.CONTROLLER.MODE.NONE
                });
            }
            return state.merge({ selected_panel_index: action.payload.index });
        }
        case 'SET_PANEL_PHOTO_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.photos[state.selected_panel_index].url = action.payload.image ? action.payload.image : '';
            return state.merge({ story: story, dirty: true });
        }
        case 'SET_PANEL_SCALE_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.photos[state.selected_panel_index].scale = action.payload.scale;
            return state.merge({ story: story, dirty: true });
        }
        case 'MOVE_PANEL_PHOTO_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.photos[state.selected_panel_index].position.x = action.payload.x;
            story.photos[state.selected_panel_index].position.y = action.payload.y;
            return state.merge({ story: story, dirty: true });
        }

        case 'SEND_STORYT_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'SEND_STORY_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'SEND_STORY_FULFILLED': {
            const stories = Immutable.asMutable(state.stories, {deep: true});
            stories.forEach((story, index) => {
                if (story.pk === action.payload.story.pk) {
                    stories.splice(index, 1);
                }
            });
            stories.unshift(action.payload.story);
            console.log(stories);
            return state.merge({ fetching: false, fetched: true, error: null, stories });
        }

        case 'CREATE_STORY_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'CREATE_STORY_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'CREATE_STORY_FULFILLED': {
            window.location.href = GLOBALS.API.API_BASE + `/story/${action.payload.story.pk}`;
            return state;
            // const stories = Immutable.asMutable(state.stories, {deep: true});
            // stories.unshift(action.payload.story);
            // return state.merge({ fetching: false, fetched: true, error: null, stories });
        }

        case 'ADD_TEXT_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            const text = {
                text: action.payload.text,
                position: {
                    x: 0.02, y: 0.01
                },
                color: '#000000',
                font_size: 24,
                font_family: 'Gloria Hallelujah'
            };
            story.texts.push(text);
            return state.merge({ story, selected_text_index: story.texts.length - 1, dirty: true });
        }
        case 'SELECT_TEXT_FULFILLED': {
            return state.merge({ selected_text_index: action.payload.index });
        }
        case 'MOVE_PANEL_TEXT_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.texts[state.selected_text_index].position.x = action.payload.x;
            story.texts[state.selected_text_index].position.y = action.payload.y;
            return state.merge({ story, dirty: true });
        }

        case 'CHANGE_PANEL_TEXT_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.texts[state.selected_text_index].text = action.payload.text;
            return state.merge({ story, dirty: true });
        }

        case 'CHANGE_PANEL_TEXT_COLOR_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.texts[state.selected_text_index].color = action.payload.color;
            return state.merge({ story, dirty: true });
        }
        case 'CHANGE_TEXT_SIZE_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.texts[state.selected_text_index].font_size = parseInt(action.payload.font_size);
            return state.merge({ story, dirty: true });
        }

        case 'SELECT_CONTROLLER_MODE': {
            return state.merge({ controller_mode: action.payload.mode, photo_mode: GLOBALS.CONTROLLER.PHOTO.NONE });
        }

        case 'SELECT_PHOTO_MODE': {
            return state.merge({ photo_mode: action.payload.mode });
        }
        case 'SELECT_TEXT_MODE': {
            return state.merge({ text_mode: action.payload.mode });
        }

        case 'REMOVE_TEXT_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            const text = {
                text: action.payload.text,
                position: {
                    x: 0.02, y: 0.01
                },
                color: '#000000',
                font_size: 24,
                font_family: 'Gloria Hallelujah'
            };
            story.texts[action.payload.index] = null;
            // story.texts.splice(action.payload.index, 1);
            return state.merge({ story, selected_text_index: null, dirty: true });
        }

        case 'CHANGE_TITLE_FULFILLED': {
            const story = Immutable.asMutable(state.story, {deep: true});
            story.title = action.payload.title;
            return state.merge({ story, dirty: true });
        }

        case 'DELETE_STORY_FULFILLED': {
            const stories = Immutable.asMutable(state.stories, {deep: true});
            stories.forEach((story, index) => {
                if (story.pk === action.payload.story.pk) {
                    stories.splice(index, 1);
                }
            });
            return state.merge({ fetching: false, fetched: true, error: null, stories });
        }

        case 'SELECT_STORY_FULFILLED': {
            // const stories = Immutable.asMutable(state.stories, {deep: true});
            let temp_story;
            state.stories.forEach((story, index) => {
                if (story.pk === action.payload.pk) {
                    temp_story = story;
                }
            });
            return state.merge({ fetching: false, fetched: true, error: null, story: temp_story });
        }

        default: {
            return state;
        }
    }
}