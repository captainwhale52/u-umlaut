import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

import cookie from 'js-cookie';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: null,
    user: null,
    searched_user: null
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'POST_LOGIN_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'POST_LOGIN_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'POST_LOGIN_FULFILLED': {
            cookie.set('authentication', action.payload.user.token);
            window.location.href = GLOBALS.API.API_BASE + '/user/';
            // const user = action.payload.user;
            // delete user['token'];
            return state;
            // return state.merge({ fetching: false, fetched: true, error: null, user });
        }

        case 'POST_REGISTER_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'POST_REGISTER_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'POST_REGISTER_FULFILLED': {
            cookie.set('authentication', action.payload.user.token);
            window.location.href = GLOBALS.API.API_BASE + '/user/';
            // const user = action.payload.user;
            // delete user['token'];
            return state;
            // return state.merge({ fetching: false, fetched: true, error: null, user });
        }

        case 'FETCH_AUTH_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_AUTH_REJECTED': {
            cookie.set('authentication', '');
            // TODO-KARL: Redirect function should be in the server side.
            if (window.location.href !== GLOBALS.API.API_BASE + '/') {
                window.location.href = GLOBALS.API.API_BASE + '/';
            }
            return state;
            // return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_AUTH_FULFILLED': {
            if (action.payload.user === null) {
                // TODO-KARL: Redirect function should be in the server side.
                if (window.location.href !== GLOBALS.API.API_BASE + '/') {
                    window.location.href = GLOBALS.API.API_BASE + '/';
                } else {
                    return state.merge({ fetching: false, fetched: false, error: null, user: action.payload.user });
                }
            }
            return state.merge({ fetching: false, fetched: true, error: null, user: action.payload.user });
        }

        case 'FETCH_LOGOUT_FULFILLED': {
            window.location.href = GLOBALS.API.API_BASE + '/';
            return state;
        }

        case 'SEARCH_USER_PENDING': {
            return state.merge({ fetching: true, error: null, searched_user: null });
        }
        case 'SEARCH_USER_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'SEARCH_USER_FULFILLED': {
            if (action.payload.user.pk !== state.user.pk) {
                return state.merge({ fetching: false, fetched: true, error: null, searched_user: action.payload.user });
            }
            return state.merge({ fetching: false, fetched: true, error: null, searched_user: null });
        }

        default: {
            return state;
        }
    }
}
