import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';

const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    photos: [],
});

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'FETCH_PHOTOS_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_PHOTOS_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_PHOTOS_FULFILLED': {
            return state.merge({ fetching: false, fetched: true, error: null, photos: action.payload.photos });
        }

        case 'POST_PHOTO_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'POST_PHOTO_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'POST_PHOTO_FULFILLED': {
            return state.merge({ fetching: false, fetched: true, error: null });
        }

        default: {
            return state;
        }
    }
}