import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';


const defaultState = Immutable({
    fetching: false,
    fetched: false,
    error: false,
    channel: null,
    channels: [],
});


export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'FETCH_CHANNELS_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_CHANNELS_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_CHANNELS_FULFILLED': {
            let channel;
            if (action.payload.channels.length) {
                channel = action.payload.channels[0];
            }
            return state.merge({ fetching: false, fetched: true, error: null, channels: action.payload.channels, channel });
        }

        case 'UPDATE_CHANNEL_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'UPDATE_CHANNEL_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'UPDATE_CHANNEL_FULFILLED': {
            const channels = Immutable.asMutable(state.channels, {deep: true});
            channels.forEach((channel, index) => {
                if (channel.pk === action.payload.channel.pk) {
                    channel.status = action.payload.channel.status;
                }
            });
            return state.merge({ fetching: false, fetched: true, error: null, channels });
        }

        case 'CREATE_CHANNEL_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'CREATE_CHANNEL_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'CREATE_CHANNEL_FULFILLED': {
            const channels = Immutable.asMutable(state.channels, {deep: true});
            channels.push(action.payload.channel);
            return state.merge({ fetching: false, fetched: true, error: null, channels });
        }

        case 'FETCH_CHANNEL_PENDING': {
            return state.merge({ fetching: true, error: null });
        }
        case 'FETCH_CHANNEL_REJECTED': {
            return state.merge({ fetching: false, fetched: false, error: action.payload.error });
        }
        case 'FETCH_CHANNEL_FULFILLED': {
            return state.merge({ fetching: false, fetched: true, error: null, channel: action.payload.channel });
        }

        default: {
            return state;
        }
    }
}