import { combineReducers } from 'redux';

import user from './reducer-user';
import friends from './reducer-friends';
import overlay from './reducer-overlay';
import photos from './reducer-photos';
import composite from './reducer-composite';
import channel from './reducer-channel';
import story from './reducer-story';
import notification from './reducer-notification';


export default combineReducers({
    user,
    friends,
    overlay,
    photos,
    composite,
    channel,
    story,
    notification
});
