import Enum from 'es6-enum';

const MODE = Enum('NONE', 'PHOTO_SELECT', 'PHOTO_EDIT', 'SHARE', 'TEXT_MOVE');


export default {
    MODE,
};
