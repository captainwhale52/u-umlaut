import Enum from 'es6-enum';

const MODE = Enum('NONE', 'PHOTO', 'TEXT', 'ILLUSTRATION', 'LAYOUT');
const PHOTO = Enum('NONE', 'ADD', 'MOVE', 'ZOOM', 'DELETE');
const TEXT = Enum('NONE', 'ADD', 'MOVE', 'ZOOM', 'DELETE');

export default {
    MODE,
    PHOTO,
    TEXT
};
