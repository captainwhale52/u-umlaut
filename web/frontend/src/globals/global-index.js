import API from './global-api';
import OVERLAY from './global-overlay';
import CONTROLLER from './global-controller';

export default {
    API,
    OVERLAY,
    CONTROLLER
};
