import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as PIXI from 'pixi.js';

import GLOBALS from './../../globals/global-index';

import OverlayComponent from './../../components/overlay/component-overlay';
// import CanvasLockComponent from './component-canvas-lock'; TODO-Karl Doesn't work with iOS.

import { selectPanel, movePanelPhoto, selectText, movePanelText } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            is_panel_created: false,
            sprites: [],
            containers: [],
            texts: [],
            highlight: []
        };
    }
    componentDidMount() {
        this.setup();
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);

        // Render new sprites.
        this.state.sprites.forEach((sprite, index) => {
            if (sprite._texture.textureCacheIds[0] !== nextProps.story.story.photos[index].url &&
                    nextProps.story.story.photos[index].url !== '') {
                const new_sprite = PIXI.Sprite.fromImage(nextProps.story.story.photos[index].url);
                this.state.containers[index].removeChild(this.state.sprites[index]);
                this.state.sprites[index] = new_sprite;
                this.state.containers[index].addChild(new_sprite);

                new_sprite.anchor = new PIXI.Point(0.5, 0.5);
                new_sprite.scale = new PIXI.Point(nextProps.story.story.photos[index].scale, nextProps.story.story.photos[index].scale);
                new_sprite.position = new PIXI.Point(nextProps.story.story.photos[index].position.x * this.canvas.clientWidth,
                        nextProps.story.story.photos[index].position.y * this.canvas.clientHeight);

                new_sprite.interactive = true;
                new_sprite.on('mousedown', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragStart(event, index);
                    }
                    this.activateSprite(index);
                }).on('touchstart', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragStart(event, index);
                    }
                    this.activateSprite(index);
                }).on('mouseup', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, index);
                    }
                }).on('mouseupoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, index);
                    }
                }).on('touchend', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, index);
                    }
                }).on('touchendoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, index);
                    }
                }).on('mousemove', this.onDragMove).on('touchmove', this.onDragMove);

                sprite = new_sprite;
            } else if (sprite._texture.textureCacheIds[0] !== nextProps.story.story.photos[index].url &&
                    nextProps.story.story.photos[index].url === '') {
                let new_sprite = PIXI.Sprite.fromImage(`${GLOBALS.API.STATIC_BASE}panel_empty.png`);
                this.state.containers[index].removeChild(this.state.sprites[index]);
                this.state.sprites[index] = new_sprite;
                this.state.containers[index].addChild(new_sprite);
            }
            // Make the selected sprite lighter.
            if (index === nextProps.story.selected_panel_index) {
                this.state.highlights[index].alpha = 1;
            } else {
                this.state.highlights[index].alpha = 0;
            }
            // Change the scale of each sprite.
            sprite.scale = new PIXI.Point(nextProps.story.story.photos[index].scale, nextProps.story.story.photos[index].scale);
        });

        // Render new texts.
        nextProps.story.story.texts.forEach((text, index) => {
            if (this.state.texts[index] !== undefined && text === null) {
                this.text_container.removeChild(this.state.texts[index]);
            } else if (this.state.texts[index] === undefined) {
                const new_text = new PIXI.Text(text.text);
                new_text.x = text.position.x * this.canvas.clientWidth;
                new_text.y = text.position.y * this.canvas.clientHeight;
                new_text.style = {
                    fill: text.color,
                    fontFamily: text.font_family,
                    fontSize: text.font_size,
                };
                this.text_container.addChild(new_text);
                this.state.texts.push(new_text);

                new_text.interactive = true;
                new_text.on('mousedown', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragStart(event, index);
                    }
                    this.activateText(index);
                }).on('touchstart', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragStart(event, index);
                    }
                    this.activateText(index);
                }).on('mouseup', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('mouseupoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('touchend', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('touchendoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('mousemove', this.onDragMove).on('touchmove', this.onDragMove);


            } else if (this.state.texts[index].text !== text.text) {
                this.state.texts[index].text = text.text;
                //TODO-Karl Multi-line text doesn't work for some unknown reason.
            } else if (this.state.texts[index].color !== text.color) {
                this.state.texts[index].style = {
                    fill: text.color,
                    fontFamily: text.font_family,
                    fontSize: text.font_size,
                };
            }
        });
    }

    render() {
        const width = window.innerWidth;
        const height = document.body.clientHeight - 58 - 50;
        // const height = parseInt(width * 8 / 6);
        // const height = parseInt(width * 17 / 11);

        let overlayComponent;
        if (this.props.overlay.activated) {
            overlayComponent = <OverlayComponent />;
        }

        return (
            <div style={{
                height: document.body.clientHeight - 58 - 50,
            }}>
                <canvas id="canvas" width={width} height={height} ref={(element) => { this.canvas = element; }} />
                {overlayComponent}
            </div>
        );
    }

    setup() {
        const resolution = window.devicePixelRatio ? window.devicePixelRatio : 2;
        const renderer = new PIXI.CanvasRenderer(this.canvas.clientWidth / resolution, this.canvas.clientHeight / resolution, {
            view: this.canvas,
            antialias: true,
            transparent: false,
            resolution: resolution,
            autoPreventDefault: false,
            preserveDrawingBuffer: true,
            // clearBeforeRender: true
        });
        // TODO-Karl This doesn't work for iOS.
        // renderer.plugins.interaction.autoPreventDefault = false;
        // this.canvas.style['touch-action'] = 'auto';

        renderer.backgroundColor = 0xffffff;

        const stage = new PIXI.Container();
        renderer.render(stage);
        stage.scale = new PIXI.Point(1 / resolution, 1 / resolution);

        this.renderer = renderer;
        this.stage = stage;

        this.panel_container = new PIXI.Container();
        this.frame_container = new PIXI.Container();
        this.highlight_container = new PIXI.Container();
        this.icon_container = new PIXI.Container();
        this.text_container = new PIXI.Container();
        this.stage.addChild(this.panel_container);
        this.stage.addChild(this.frame_container);
        this.stage.addChild(this.highlight_container);
        this.stage.addChild(this.icon_container);
        this.stage.addChild(this.text_container);

        this.update();
    }

    update = () => {
        this.renderer.render(this.stage);
        if (!this.state.is_panel_created) {
            this.setState({
                is_panel_created: true,
            });
            this.renderPanels();
            this.renderTexts();
        }
        requestAnimationFrame(this.update);
    };

    renderTexts = () => {
        this.props.story.story.texts.forEach((text, index) => {
            if (this.state.texts[index] === undefined) {
                const new_text = new PIXI.Text(text.text);
                new_text.x = text.position.x * this.canvas.clientWidth;
                new_text.y = text.position.y * this.canvas.clientHeight;
                new_text.style = {
                    fill: text.color,
                    fontFamily: text.font_family,
                    fontSize: text.font_size,
                };
                this.text_container.addChild(new_text);
                this.state.texts.push(new_text);

                new_text.interactive = true;
                new_text.on('mousedown', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragStart(event, index);
                    }
                    this.activateText(index);
                }).on('touchstart', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragStart(event, index);
                    }
                    this.activateText(index);
                }).on('mouseup', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('mouseupoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('touchend', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.TEXT) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('touchendoutside', (event) => {
                    if (this.props.overlay.mode === GLOBALS.OVERLAY.MODE.TEXT_MOVE) {
                        this.onTextDragEnd(event, index);
                    }
                    this.activateText(index);
                }).on('mousemove', this.onDragMove).on('touchmove', this.onDragMove);
            }
        });
    };

    renderPanels = () => {  // Assume stage is already bind to the renderer.
        const panel_line_width = this.props.story.story.properties.line_width;
        const sprites = [];
        const containers = [];
        const highlights = [];
        this.props.story.story.panels.forEach((panel, panel_index) => {
            const container = new PIXI.Container();
            containers.push(container);
            const mask = new PIXI.Graphics();
            const background = new PIXI.Graphics();
            let sprite;
            if (this.props.story.story.photos[panel_index].url === '') {
                sprite = PIXI.Sprite.fromImage(`${GLOBALS.API.STATIC_BASE}panel_empty.png`);
            } else {
                sprite = PIXI.Sprite.fromImage(this.props.story.story.photos[panel_index].url);
            }

            sprites.push(sprite);

            container.addChild(mask);
            container.addChild((background));
            container.addChild(sprite);
            container.mask = mask;
            this.panel_container.addChild(container);


            const frame = new PIXI.Graphics();
            const highlight = new PIXI.Graphics();
            highlights.push(highlight);

            this.highlight_container.addChild(highlight);
            this.frame_container.addChild(frame);

            // Add a mask panel to cut out the sprite.
            mask.beginFill(0xffffff, 1);
            background.beginFill(0xffffff, 1);
            // mask.lineStyle(panel_line_width, 0x123123);
            frame.lineStyle(2, 0x000000);
            highlight.lineStyle(6, 0x4baff1);
            highlight.alpha = 0;

            // TODO-Karl: Calculate the center position of the panel (assume every panel is convex form).
            let total_width = 0;
            let total_height = 0;
            panel.points.forEach((point, point_index) => {
                total_width += point.x;
                total_height += point.y;
            });
            const center = new PIXI.Point(total_width / panel.points.length, total_height / panel.points.length);

            let first_point, second_point;
            panel.points.forEach((point, point_index) => {
                let x = 0, y = 0;
                if (point.x === 0) {
                    x = point.x * this.canvas.clientWidth + panel_line_width * 2;
                } else if (point.x === 1) {
                    x = point.x * this.canvas.clientWidth - panel_line_width * 2;
                } else if (point.x === center.x) {
                    x = point.x * this.canvas.clientWidth;
                }  else {
                    x = point.x < center.x ?
                        point.x * this.canvas.clientWidth + panel_line_width :
                        point.x * this.canvas.clientWidth - panel_line_width;
                }
                if (point.y === 0) {
                    y = point.y * this.canvas.clientHeight + panel_line_width * 2;
                } else if (point.y === 1) {
                    y = point.y * this.canvas.clientHeight - panel_line_width * 2;
                } else if (point.y === center.y) {
                    y = point.y * this.canvas.clientHeight;
                } else {
                    y = point.y < center.y ?
                        point.y * this.canvas.clientHeight + panel_line_width :
                        point.y * this.canvas.clientHeight - panel_line_width;
                }
                if (point_index === 0) {
                    mask.moveTo(x, y);
                    background.moveTo(x, y);
                    frame.moveTo(x, y);
                    highlight.moveTo(x, y);
                    first_point = new PIXI.Point(x, y);
                } else if (point_index === 1) {
                    mask.lineTo(x, y);
                    background.lineTo(x, y);
                    frame.lineTo(x, y);
                    highlight.lineTo(x, y);
                    second_point = new PIXI.Point(x, y);
                } else {
                    mask.lineTo(x, y);
                    background.lineTo(x, y);
                    frame.lineTo(x, y);
                    highlight.lineTo(x, y);
                    if (point_index === panel.points.length - 1) {
                        frame.lineTo(first_point.x, first_point.y);
                        highlight.lineTo(first_point.x, first_point.y);
                        frame.lineTo(second_point.x, second_point.y);
                        highlight.lineTo(second_point.x, second_point.y);
                    }
                }
            });


            // Add a touch input on the sprite.
            background.interactive = true;
            background.on('mousedown', (event) => {
                this.activateSprite(panel_index);
            }).on('touchstart', (event) => {
                this.activateSprite(panel_index);
            });

            sprite.anchor = new PIXI.Point(0.5, 0.5);
            sprite.scale = new PIXI.Point(this.props.story.story.photos[panel_index].scale, this.props.story.story.photos[panel_index].scale);
            sprite.position = new PIXI.Point(this.props.story.story.photos[panel_index].position.x * this.canvas.clientWidth,
                        this.props.story.story.photos[panel_index].position.y * this.canvas.clientHeight);

            // Add touch event listener on the non-default sprite.
            if (this.props.story.story.photos[panel_index].url !== '') {
                sprite.interactive = true;
                sprite.on('mousedown', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragStart(event, panel_index);
                    }
                    this.activateSprite(panel_index);
                }).on('touchstart', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragStart(event, panel_index);
                    }
                    this.activateSprite(panel_index);
                }).on('mouseup', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, panel_index);
                    }
                }).on('mouseupoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, panel_index);
                    }
                }).on('touchend', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, panel_index);
                    }
                }).on('touchendoutside', (event) => {
                    if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
                        this.onDragEnd(event, panel_index);
                    }
                }).on('mousemove', this.onDragMove).on('touchmove', this.onDragMove);
            }
        });

        this.setState({
            sprites,
            containers,
            highlights
        });
    };

    activateSprite = (index) => {
        console.log("!!! ACTIVATE SPRITRE " + index);
        if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.NONE) {
            // if (this.props.story.selected_panel_index === index) {
            //     this.props.selectPanel(null);
            // } else {
            //
            // }
            this.props.selectPanel(index);
        } else if (this.props.story.controller_mode === GLOBALS.CONTROLLER.MODE.PHOTO) {
            // if (this.props.story.selected_panel_index === index) {
            //     this.props.selectPanel(null);
            // } else {
            //
            // }
            this.props.selectPanel(index);
        }

    };

    activateSpriteForPhotoMove = (index) => {
        this.props.selectPanel(index);
    };

    activateText = (index) => {
        this.props.selectText(index);
    };

    onDragStart(event, index) {
        this.activateSpriteForPhotoMove(index);
        const sprite = this.state.sprites[index];
        sprite.data = event.data;
        sprite.dragging = sprite.data.getLocalPosition(sprite.parent);
    };

    onDragEnd(event, index) {
        const sprite = this.state.sprites[index];
        sprite.dragging = false;
        sprite.data = null;
        this.props.movePanelPhoto(sprite.position.x / this.canvas.clientWidth, sprite.position.y / this.canvas.clientHeight);
    };

    onDragMove() {
        if (this.dragging) {
            const newPosition = this.data.getLocalPosition(this.parent);
            this.position.x += (newPosition.x - this.dragging.x);
            this.position.y += (newPosition.y - this.dragging.y);
            this.dragging = newPosition;
        }
    };

    onTextDragStart(event, index) {
        const text = this.state.texts[index];
        text.data = event.data;
        text.dragging = text.data.getLocalPosition(text.parent);
    };

    onTextDragEnd(event, index) {
        const text = this.state.texts[index];
        text.dragging = false;
        text.data = null;
        this.props.movePanelText(text.position.x / this.canvas.clientWidth, text.position.y / this.canvas.clientHeight);
    };
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        overlay: state.overlay,
        composite: state.composite,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPanel,
        movePanelPhoto,
        selectText,
        movePanelText
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);