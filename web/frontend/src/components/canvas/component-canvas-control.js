import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';
import { updateStory } from './../../events/actions/action-story';


import GLOBALS from './../../globals/global-index';

import MainControllerComponent from './../controller/component-controller-main';
import PhotoControllerComponent from './../controller/component-controller-photo';
import TextControllerComponent from './../controller/component-controller-text';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        let controller;
        switch(this.props.story.controller_mode) {
            case GLOBALS.CONTROLLER.MODE.PHOTO: {
                controller = <PhotoControllerComponent />;
                break;
            }
            case GLOBALS.CONTROLLER.MODE.TEXT: {
                controller = <TextControllerComponent />;
                break;
            }
            default: {
                controller = <MainControllerComponent />;
                break;
            }
        }
        return <div className={this.context.styles['control']}>
            {controller}
            {/*<a href={GLOBALS.API.API_BASE + `/channel/${this.props.story.story.channel}`}><button>Back</button></a>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.setOverlayActivated(true);*/}
            {/*}}>Edit</button>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.updateStory();*/}
            {/*}}>Save</button>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.setOverlayActivated(true);*/}
                {/*this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.SHARE);*/}
            {/*}}>Share</button>*/}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setOverlayActivated,
        setOverlayMode,
        updateStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);