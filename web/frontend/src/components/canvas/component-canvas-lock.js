import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';

import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            is_locked: false,
        };
    }
    componentWillMount() {

    }

    render() {
        let action;
        if (this.state.is_locked) {
            action = (<button onClick={() => {
                this.setState({
                    is_locked: false,
                });
                this.props.toggle_lock(false);
            }}>SCROLL<br/>LOCKED</button>);
        } else {
            action = (<button onClick={() => {
                this.setState({
                    is_locked: true,
                });
                this.props.toggle_lock(true);
            }}>SCROLL<br/>UNLOCKED</button>);
        }
        return <div className={this.context.styles['canvas-lock']}>
            {action}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        // composite: state.composite,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        // setOverlayActivated,
        // setOverlayMode
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);