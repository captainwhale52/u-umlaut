import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import * as PIXI from 'pixi.js';

// import store from '../../events/stores/store-index';


// import GLOBALS from '../../globals/global-index';
//
// import Character_1 from './../../components/characters/character_1';
// import Apple_1 from './../../components/objects/apple_1';
//
//
// import { setCharacter1Direction, setCharacter1Locomotion } from './../../events/actions/action-character_1';
// import { setApple1Position, setApple1Belonged } from './../../events/actions/action-apple_1';
// import { processTextInput, processAudioInput } from './../../events/actions/action-input';

import  { fetchLogout } from './../../events/actions/action-user';


class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let info;
        if (!this.props.user.user) {
            return null;
        }
        return <div>
            <div>
                <span>User Information</span>
            </div>
            <div>
                <span>Name: </span> <span>{`${this.props.user.user.first_name} ${this.props.user.user.last_name}`}</span>
            </div>
            <div>
                <span>Email: </span> <span>{this.props.user.user.email}</span>
            </div>
            <button onClick={() => {
                this.props.fetchLogout();
            }}>Logout</button>
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchLogout
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);