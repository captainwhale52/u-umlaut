import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  { postLogin } from './../../events/actions/action-user';

class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <div>
                    <span>Login</span>
                </div>
                <div>
                    <label htmlFor="login-username">Username: </label>
                    <input id="login-username" type="text" autoCapitalize="none" ref={(element) => { this.input_username = element; }} />
                </div>
                <div>
                    <label htmlFor="login-password">Password: </label>
                    <input id="login-password" type="password" ref={(element) => { this.input_password = element; }} />
                </div>
                <button onClick={() => {
                    this.props.postLogin(this.input_username.value, this.input_password.value);
                }}>Login</button>
                <hr />
            </div>
        )
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        postLogin
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);