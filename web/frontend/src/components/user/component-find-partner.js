import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectPhotoMode, setPanelPhoto } from './../../events/actions/action-story';
import { fetchPhotos, postPhoto } from './../../events/actions/action-photo';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            selected_image: null,
        };
    }
    componentDidMount() {

    }

    render() {
        // const photos = this.props.photos.photos.map((photo, index) => {
        //     if (this.state.selected_image === photo.image) {
        //         return (
        //             <div key={`photo-${photo.pk}`} className={this.context.styles['photo_item']} style={{
        //                 backgroundImage: `url("${photo.image}")`,
        //                 border: `4px solid rgba(70,4,130,1)`
        //             }} onClick={() => {
        //                 this.setState({
        //                     selected_image: photo.image
        //                 });
        //             }} />
        //         );
        //     } else {
        //         return (
        //             <div key={`photo-${photo.pk}`} className={this.context.styles['photo_item']} style={{
        //                 backgroundImage: `url("${photo.image}")`,
        //             }} onClick={() => {
        //                 this.setState({
        //                     selected_image: photo.image
        //                 });
        //             }} />
        //         );
        //     }
        //
        // });

        return <div className={this.context.styles['overlay']}>
            <div className={this.context.styles['container']}>
                <div className={this.context.styles['header']}>
                    <div className={this.context.styles['left']}>
                        <button onClick={() => {
                            this.props.selectPhotoMode(GLOBALS.CONTROLLER.MODE.NONE);
                        }}>
                            <img src={`${GLOBALS.API.STATIC_BASE}btn_close.png`} />
                        </button>
                    </div>
                    <div className={this.context.styles['right']}>
                        <button onClick={() => {
                            this.props.setPanelPhoto(this.state.selected_image).then(() => {
                                this.props.selectPhotoMode(GLOBALS.CONTROLLER.MODE.NONE);
                            });
                        }}>
                            Select
                        </button>
                    </div>

                </div>
                <div className={this.context.styles['body']}>

                </div>
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        photos: state.photos,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPhotoMode,
        fetchPhotos,
        setPanelPhoto,
        postPhoto,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);