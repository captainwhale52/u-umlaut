import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  { postRegister } from './../../events/actions/action-user';

class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <div>
                    <span>Register</span>
                </div>
                <div>
                    <label htmlFor="login_username">Username: </label>
                    <input id="login_username" type="text" autoCapitalize="none" ref={(element) => { this.input_username = element; }} />
                </div>
                <div>
                    <label htmlFor="login_email">Email: </label>
                    <input id="login_email" type="email" ref={(element) => { this.input_email = element; }} />
                </div>
                <div>
                    <label htmlFor="login_first_name">First name: </label>
                    <input id="login_first_name" type="text" ref={(element) => { this.input_first_name = element; }} />
                </div>
                <div>
                    <label htmlFor="login_last_name">Last name: </label>
                    <input id="login_last_name" type="text" ref={(element) => { this.input_last_name = element; }} />
                </div>
                <div>
                    <label htmlFor="login_password">Password: </label>
                    <input id="login_password" type="password" ref={(element) => { this.input_password = element; }} />
                </div>
                <button onClick={() => {
                    this.props.postRegister(
                        this.input_username.value,
                        this.input_email.value,
                        this.input_first_name.value,
                        this.input_last_name.value,
                        this.input_password.value
                    );
                }}>Register</button>
                <hr />
            </div>
        )
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        postRegister
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);