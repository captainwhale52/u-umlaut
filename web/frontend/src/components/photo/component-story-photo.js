import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectPhotoMode, setPanelPhoto, selectStory, uploadStory } from './../../events/actions/action-story';
import { fetchPhotos, postPhoto } from './../../events/actions/action-photo';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            selected_image: null,
        };
    }
    componentDidMount() {

    }

    render() {
        // const recipient = this.props.user.user.pk === this.props.channel.recipient.pk ? 'You' : `${this.props.channel.recipient.first_name} ${this.props.channel.recipient.last_name}`;
        //
        // const requester = this.props.user.user.pk === this.props.channel.requester.pk ? 'You' : `${this.props.channel.requester.first_name} ${this.props.channel.requester.last_name}`;

        return <div className={this.context.styles['overlay']}>
            <div className={this.context.styles['container']}>
                <div className={this.context.styles['header']}>
                    <div className={this.context.styles['left']}>
                        <div className={this.context.styles['title']}>{this.props.story.story.title}
                        </div>
                        <div className={this.context.styles['author']}>
                            Written by
                            <span>{this.props.channel.channel.requester.first_name} {this.props.channel.channel.requester.last_name}</span> &
                            <span>{this.props.channel.channel.recipient.first_name} {this.props.channel.channel.recipient.last_name}</span>
                        </div>
                    </div>
                    <div className={this.context.styles['right']}>
                        <button onClick={() => {
                            this.props.selectStory(null);
                        }}>
                            <img src={`${GLOBALS.API.STATIC_BASE}btn_close.png`} />
                        </button>
                        {/*<button onClick={() => {*/}
                            {/*this.props.setPanelPhoto(this.state.selected_image).then(() => {*/}
                                {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.MODE.NONE);*/}
                            {/*});*/}
                        {/*}}>*/}
                            {/*Select*/}
                        {/*</button>*/}
                    </div>

                </div>
                <div className={this.context.styles['body']}>
                    <div className={this.context.styles['thumbnail']}>
                        <img src={this.props.story.story.thumbnail} />
                    </div>
                    <div className={this.context.styles['actions']}>
                        <a ref={(element) => { this.download_story = element; }}>
                            <button style={{
                                filter: 'grayscale(100%)',
                                opacity: 0.25
                            }} onClick={(event) => {
                                // this.download_story.href = this.props.story.story.thumbnail;
                                // this.download_story.download = this.props.story.story.title;
                            }}>Download</button>
                        </a>
                        <a>
                            <button style={{
                                filter: 'grayscale(100%)',
                                opacity: 0.25
                            }} onClick={() => {
                                // const self = this;
                                // FB.getLoginStatus(function(response) {
                                //     if (response.status === 'connected') {
                                //         FB.login(function(){
                                //             // console.log('Welcome!  Fetching your information.... ');
                                //             // FB.api('/me/feed', 'POST', {
                                //             //     message: self.props.story.story.title,
                                //             //     link: self.props.story.story.thumbnail,
                                //             // },
                                //             // function (response) {
                                //             //   if (response && !response.error) {
                                //             //       console.log(response);
                                //             //     /* handle the result */
                                //             //   }
                                //             // });
                                //             FB.ui({
                                //                 method: 'share',
                                //                 href: self.props.story.story.thumbnail,
                                //             }, function(response){});
                                //         }, {scope: "publish_actions"});
                                //     } else {
                                //         FB.login(function(response) {
                                //             if (response.authResponse) {
                                //                 // console.log('Welcome!  Fetching your information.... ');
                                //                 // FB.api('/me/feed', 'POST', {
                                //                 //     message: self.props.story.story.title,
                                //                 //     link: self.props.story.story.thumbnail,
                                //                 // },
                                //                 // function (response) {
                                //                 //   if (response && !response.error) {
                                //                 //       console.log(response);
                                //                 //     /* handle the result */
                                //                 //   }
                                //                 // });
                                //                 FB.ui({
                                //                     method: 'share',
                                //                     href: self.props.story.story.thumbnail,
                                //                 }, function(response){});
                                //             } else {
                                //             // console.log('User cancelled login or did not fully authorize.');
                                //             }
                                //         });
                                //     }
                                // });
                            }}>Facebook</button>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        channel: state.channel,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPhotoMode,
        fetchPhotos,
        setPanelPhoto,
        postPhoto,
        selectStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);