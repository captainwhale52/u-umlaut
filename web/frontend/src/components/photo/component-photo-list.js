import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchPhotos } from './../../events/actions/action-photo';
import PhotoItem from './component-photo-item';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        this.props.fetchPhotos();
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        const photos = this.props.photos.photos.map((photo, index) => {
            return <PhotoItem key={`photo-${photo.pk}`} photo={photo} />;
        });
        return <div>
            <span>Photo List</span>
            <hr />
            <div className={this.context.styles['photo-list']}>
                {photos}
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        photos: state.photos
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchPhotos
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);