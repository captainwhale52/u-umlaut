import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { setCompositePhotoByIndex } from './../../events/actions/action-composite';
import * as moment from 'moment';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        return <div className={this.context.styles['photo-item']}>
            <div><span>{moment(this.props.photo.taken_at).format('MMMM Do YYYY, h:mm:ss a')}</span></div>
            <img src={this.props.photo.image} onClick={() => {

            }} />
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        // setCompositePhotoByIndex
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);