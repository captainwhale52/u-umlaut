import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { setCompositePhotoByIndex } from './../../events/actions/action-composite';
import * as moment from 'moment';

import GLOBALS from './../../globals/global-index';

import  { removeNotification } from './../../events/actions/action-notification';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            is_loaded: true,
        };
    }
    componentDidMount() {
        // setTimeout(() => {
        //     this.setState({
        //        is_loaded: true,
        //     });
        // }, 5000);
    }

    componentWillUpdate(nextProps) {

    }

    render() {
        if (this.state.is_loaded) {
            const styles = [this.context.styles['notification-popup']];
            if (this.props.notification.notifications.length) {
                let content;
                const item = this.props.notification.notifications[this.props.notification.notifications.length - 1];

                switch(item.action) {
                    case 'CHANNEL': {
                        const recipient = this.props.user.user.pk === item.recipient.pk ? 'You' : `${item.recipient.first_name} ${item.recipient.last_name}`;
                        const requester = this.props.user.user.pk === item.requester.pk ? 'You' : `${item.requester.first_name} ${item.requester.last_name}`;
                        if (item.status === 'AP') {
                            if (this.props.user.user.pk === item.requester.pk) {
                                content = (
                                    <div className={this.context.styles['container']}>
                                        {recipient + ' has accepted your request.'}
                                    </div>
                                );
                            } else if (this.props.user.user.pk === item.recipient.pk) {
                                return null;
                            }
                        }
                        return null;
                    }

                    case 'STORY': {
                        if (this.props.user.user.pk === item.editor.pk) {
                            let from;
                            if (this.props.channel.channel.recipient.pk === this.props.user.user.pk) {
                                from = this.props.channel.channel.requester.first_name + ' ' + this.props.channel.channel.requester.last_name;
                            } else {
                                from = this.props.channel.channel.recipient.first_name + ' ' + this.props.channel.channel.recipient.last_name;
                            }
                            content = (
                                <div className={this.context.styles['container']} onClick={() => {
                                    window.location.href = GLOBALS.API.API_BASE + `/story/${item.pk}`;
                                }}>
                                    <div className={this.context.styles['title']}>
                                        You have a new message.
                                    </div>
                                    <div className={this.context.styles['content']}>
                                        <span className={this.context.styles['name']}>{from}</span> just sent you a new story.
                                        <span className={this.context.styles['click']}>Click</span> to check out!
                                    </div>

                                </div>
                            );
                        } else {
                            return null;
                        }
                    }

                    default: {

                        break;
                    }
                }
                return (<div className={styles}>
                    {content}
                </div>);
            }
        }
        return null;
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        channel: state.channel,
        user: state.user,
        notification: state.notification
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        removeNotification
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);