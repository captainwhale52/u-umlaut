import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from '../../globals/global-index';

import { setOverlayMode, setOverlayActivated } from './../../events/actions/action-overlay';
import { selectPanel, setPanelPhoto } from './../../events/actions/action-story';


import PhotoList from './component-photo-list';
import PhotoEdit from './component-photo-edit';
import Share from './../share/component-share';
import TextEdit from './component-text-edit';



class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        let content;
        let title = <div>
            <span>{`Panel Editor - #${this.props.story.selected_panel_index}`}</span>
            <hr />
        </div>
        switch (this.props.overlay.mode) {
            case GLOBALS.OVERLAY.MODE.PHOTO_SELECT:
                content = <PhotoList />;
                break;
            case GLOBALS.OVERLAY.MODE.PHOTO_EDIT:
                content = <PhotoEdit />;
                break;
            case GLOBALS.OVERLAY.MODE.SHARE:
                content = <Share />;
                title = null;
                break;
            case GLOBALS.OVERLAY.MODE.TEXT_MOVE:
                content = <TextEdit />;
            default:
                break;
        }
        let buttons;
        switch(this.props.overlay.mode) {
            case GLOBALS.OVERLAY.MODE.PHOTO_SELECT:
                buttons = <div>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_EDIT);
                    }}>Edit Image</button>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.TEXT_MOVE);
                    }}>Edit Text</button>
                    <button onClick={() => {
                        this.props.setPanelPhoto(null);
                    }}>Remove Photo</button>
                    <button onClick={() => {
                        this.props.selectPanel(-1);
                        this.props.setOverlayActivated(false);
                    }}>Close</button>
                </div>;
                break;
            case GLOBALS.OVERLAY.MODE.PHOTO_EDIT:
                buttons = <div>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_SELECT);
                    }}>Select Image</button>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.TEXT_MOVE);
                    }}>Edit Text</button>
                    <button onClick={() => {
                        this.props.selectPanel(-1);
                        this.props.setOverlayActivated(false);
                    }}>Close</button>
                </div>;
                break;
            case GLOBALS.OVERLAY.MODE.SHARE:
                break;
            case GLOBALS.OVERLAY.MODE.TEXT_MOVE:
                buttons = <div>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_SELECT);
                    }}>Select Image</button>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_EDIT);
                    }}>Edit Image</button>
                    <button onClick={() => {
                        this.props.selectPanel(-1);
                        this.props.setOverlayActivated(false);
                    }}>Close</button>
                </div>;
                break;
            default:
                buttons = <div>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_SELECT);
                    }}>Select Image</button>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.PHOTO_EDIT);
                    }}>Edit Image</button>
                    <button onClick={() => {
                        this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.TEXT_MOVE);
                    }}>Edit Text</button>
                    <button onClick={() => {
                        this.props.setOverlayActivated(false);
                    }}>Close</button>
                </div>;
                break;
        }

        return <div className={this.context.styles['overlay']}>
            {title}
            {buttons}
            {content}
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
        overlay: state.overlay,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setOverlayMode,
        setOverlayActivated,
        selectPanel,
        setPanelPhoto
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);