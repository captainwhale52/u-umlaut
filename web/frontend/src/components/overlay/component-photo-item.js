import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setPanelPhoto } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        return <div className={this.context.styles['photo-item']}>
            <img src={this.props.photo.image} onClick={() => {
                this.props.setPanelPhoto(this.props.photo.image);
            }} />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setPanelPhoto
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);