import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchPhotos } from './../../events/actions/action-photo';
import PhotoItem from './component-photo-item';

// import { setTextColorByIndex } from './../../events/actions/action-composite';
import { addText, changePanelText, changePanelTextColor, changeTextSize } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        if (this.props.composite.editing_text_index !== null) {
            this.input_text.value = this.props.composite.texts[this.props.composite.editing_text_index].text;
        }
    }

    // componentWillUpdate(nextProps, nextState) {
    //     if (nextProps.composite.editing_text_index !== null) {
    //         console.log(this.input_text);
    //         if (this.input_text) {
    //             console.log(this.input_text);
    //             console.log(this.input_text.value);
    //             this.input_text.value = nextProps.composite.texts[nextProps.composite.editing_text_index].text;
    //         }
    //
    //     }
    // }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.story.selected_text_index !== null) {
            if (this.input_text) {
                this.input_text.value = this.props.story.story.texts[this.props.story.selected_text_index].text;
            }
            if (this.input_color) {
                this.input_color.value = this.props.story.story.texts[this.props.story.selected_text_index].color;
            }
            if (this.input_font_size) {
                this.input_font_size.value = this.props.story.story.texts[this.props.story.selected_text_index].font_size;

            }
        }
    }

    render() {
        let content;
        if (this.props.story.selected_text_index === null) {
            content = <div>
                <div>
                    <span>Please select a text to edit.</span>
                </div>
                <hr/>
                <div>
                    <span>New Text</span>
                </div>
                <div>
                    <label htmlFor="input_new_text">New Text: </label>
                    <input id="input_new_text" type="text" ref={(element) => { this.input_new_text = element; }} />
                    <button onClick={() => {
                        this.props.addText(this.input_new_text.value);
                    }}>Add</button>
                </div>
            </div>
        } else {
            content = <div>
                <div>
                    <span>You can drag a text to position as you wish.</span>
                </div>
                <div>
                    <label htmlFor="input_text">Text: </label>
                    <input id="input_text" type="text" ref={(element) => { this.input_text = element; }} onChange={(event) => {
                        this.props.changePanelText(event.target.value);
                    }} />
                    <input type="color"
                           ref={(element) => { this.input_color = element; }}
                           onChange={(event) => {
                        this.props.changePanelTextColor(event.target.value);
                    }} />
                </div>
                <div>
                    <label htmlFor="input_font_size_level">Font size: </label>
                    <input id="input_font_size_level" type="range" min="10" max="100" step="1" ref={(element) => { this.input_font_size = element; }}
                        onChange={(event) => {
                            // const scale = Math.round(Math.min(Math.max(Math.pow(1.3, event.target.value) - 1, 0.1), 2) * 100) / 100;
                            // console.log(event.target.value);
                            this.props.changeTextSize(event.target.value);
                            // this.props.SetPanelScale(scale);
                        }
                    } />
                </div>
                <hr/>
                <div>
                    <span>New Text</span>
                </div>
                <div>
                    <label htmlFor="input_new_text">New text: </label>
                    <input id="input_new_text" type="text" ref={(element) => { this.input_new_text = element; }} />
                    <button onClick={() => {
                        this.props.addText(this.input_new_text.value);
                        // this.props.setCompositeZoomInByIndex(this.props.composite.editing_panel_index);
                    }}>Add</button>
                </div>
            </div>
        }
        return <div>
            <div>
                <span>Text Edit</span>
            </div>
            {content}
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changePanelText,
        changePanelTextColor,
        addText,
        changeTextSize
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);