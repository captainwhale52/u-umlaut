import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchPhotos } from './../../events/actions/action-photo';
import PhotoItem from './component-photo-item';

import { SetPanelScale } from './../../events/actions/action-story';



class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {


    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        let default_scale_level = Math.log(this.props.story.story.photos[this.props.story.selected_panel_index].scale + 1) / Math.log(1.3);
        return <div>
            <div>
                <span>Photo Edit</span>
            </div>
            <div>
                <span>You can drag an image to position as you wish.</span>
            </div>
            <div>
                <label htmlFor="input_zoom_level">Zoom level: </label>
                <input id="input_zoom_level" type="range" min="1" max="4" step="0.01" value={default_scale_level}
                    onChange={(event) => {
                        const scale = Math.round(Math.min(Math.max(Math.pow(1.3, event.target.value) - 1, 0.1), 2) * 100) / 100;
                        this.props.SetPanelScale(scale);
                    }
                } />
                {/*<button onClick={() => {*/}
                    {/*this.props.setCompositeZoomInByIndex(this.props.composite.editing_panel_index);*/}
                {/*}}>Zoom In</button>*/}
                {/*<button onClick={() => {*/}
                    {/*this.props.setCompositeZoomOutByIndex(this.props.composite.editing_panel_index);*/}
                {/*}}>Zoom Out</button>*/}
            </div>
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        composite: state.composite,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        SetPanelScale
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);