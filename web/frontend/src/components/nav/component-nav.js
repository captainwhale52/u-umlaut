import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    shouldComponentUpdate(nextProps, nextState) {

    }
    render() {
        return <div>
            <div>Ü</div>
            <a href={GLOBALS.API.API_BASE + '/'}><button >Home</button></a>
            <a href={GLOBALS.API.API_BASE + '/photo/'}><button>Upload Photo</button></a>
            <hr />
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);