import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';
import { updateStory } from './../../events/actions/action-story';


import GLOBALS from './../../globals/global-index';

import MainControllerComponent from './../controller/component-controller-main';
import PhotoControllerComponent from './../controller/component-controller-photo';
import TextControllerComponent from './../controller/component-controller-text';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        return <div className={this.context.styles['control']}>
            <button onClick={() => {
                window.location.href = GLOBALS.API.API_BASE + '/photo/';
                // this.props.selectControllerMode(GLOBALS.CONTROLLER.MODE.NONE);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_photo_lib.png`} />
            </button>
            <button onClick={() => {
                // this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.ADD);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_story_on.png`} />
            </button>
            {/*<button onClick={() => {*/}
                {/*if (this.props.story.photo_mode !== GLOBALS.CONTROLLER.PHOTO.MOVE) {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.MOVE);*/}
                {/*} else {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.NONE);*/}
                {/*}*/}
            {/*}}>*/}
                {/*<img src={`${GLOBALS.API.STATIC_BASE}btn_move.png`} />*/}
            {/*</button>*/}
            <button onClick={() => {
                // this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.ZOOM);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_notification.png`} style={{
                    filter: 'grayscale(100%)',
                    opacity: 0.25
                }}/>
            </button>
            <button onClick={() => {
                // this.props.setPanelPhoto(null);
                window.location.href = GLOBALS.API.API_BASE + `/user/`;
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_connect.png`} />
            </button>

            {/*<a href={GLOBALS.API.API_BASE + `/channel/${this.props.story.story.channel}`}><button>Back</button></a>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.setOverlayActivated(true);*/}
            {/*}}>Edit</button>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.updateStory();*/}
            {/*}}>Save</button>*/}
            {/*<button onClick={() => {*/}
                {/*this.props.setOverlayActivated(true);*/}
                {/*this.props.setOverlayMode(GLOBALS.OVERLAY.MODE.SHARE);*/}
            {/*}}>Share</button>*/}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);