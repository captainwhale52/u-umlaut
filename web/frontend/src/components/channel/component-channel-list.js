import Immutable from 'seamless-immutable';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  { fetchChannels } from './../../events/actions/action-channel';
import  { addNotification } from './../../events/actions/action-notification';
import ChannelItem from './component-channel-item';


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentWillMount() {
        this.props.fetchChannels();
        setInterval(() => {
            this.props.fetchChannels();
        }, 10000);
    }

    componentWillUpdate(nextProps) {
        nextProps.channel.channels.filter((new_channels, index) => {
            let is_new = true;
            this.props.channel.channels.filter((old_channels, index) => {
                if (new_channels.pk === old_channels.pk && new_channels.updated_at === old_channels.updated_at) {
                    is_new = false;
                }
            });
            if (is_new) {
                const channel = Immutable.asMutable(new_channels, {deep: true});
                channel.action = 'CHANNEL';
                this.props.addNotification(channel);
            }
        });
    }

    render() {
        const channels = this.props.channel.channels.map((channel, index) => {
            return <ChannelItem key={`channel-${channel.pk}`} channel={channel} />;
        });
        return <div>
            <div>
                CHANNEL LIST
            </div>
            <div>
                {channels}
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        channel: state.channel
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchChannels,
        addNotification
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);