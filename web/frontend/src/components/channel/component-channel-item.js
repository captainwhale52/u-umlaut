import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { setCompositePhotoByIndex } from './../../events/actions/action-composite';
import * as moment from 'moment';

import GLOBALS from './../../globals/global-index';


import  { updateChannel } from './../../events/actions/action-channel';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let channel_type = 'N/A';
        switch(this.props.channel.type) {
            case 'LV': {
                channel_type = 'Lover';
                break;
            }
            case 'FD': {
                channel_type = 'Friend';
                break;
            }
            case 'FM': {
                channel_type = 'Family';
                break;
            }
            case 'CW': {
                channel_type = 'Colleague';
                break;
            }
            default: {
                break;
            }
        }


        switch(this.props.channel.status) {
            case 'AP': {
                const recipient = this.props.user.user.pk === this.props.channel.recipient.pk ? 'You' : `${this.props.channel.recipient.first_name} ${this.props.channel.recipient.last_name}`;

                const requester = this.props.user.user.pk === this.props.channel.requester.pk ? 'You' : `${this.props.channel.requester.first_name} ${this.props.channel.requester.last_name}`;
                return <div className={this.context.styles['channel-item']}>
                    <div>Story between <span>{requester} & {recipient}</span></div>
                    <div>Status: <span>Active</span></div>
                    <div>Type: <span>{channel_type}</span></div>
                    <div>
                        <a href={GLOBALS.API.API_BASE + `/channel/${this.props.channel.pk}`}><button >Select</button></a>
                    </div>
                </div>
            }
            case 'WT': {
                if (this.props.user.user.pk === this.props.channel.requester.pk) {  // The user is the requester.
                    return <div className={this.context.styles['channel-item']}>
                        <div>Waiting for a response from <span>{this.props.channel.recipient.first_name} {this.props.channel.recipient.last_name}</span></div>
                        <div>Status: <span>Waiting</span></div>
                        <div>Type: <span>{channel_type}</span></div>
                    </div>
                } else { // The user is the recipient.
                    return <div className={this.context.styles['channel-item']}>
                        <div>Request from <span>{this.props.channel.requester.first_name} {this.props.channel.requester.last_name}</span></div>
                        <div>Status: <span>Waiting</span></div>
                        <div>Type: <span>{channel_type}</span></div>
                        <div>
                            <button onClick={() => {
                                this.props.updateChannel(this.props.channel.pk, 'AP');  // TODO-Karl: Convert string values into ENUM.
                            }}>Accept</button>
                        </div>
                    </div>
                }
            }
            default: {
                return null;
            }
        }

        // if (this.props.channel.status == 'AP') {
        //     return <div className={this.context.styles['channel-item']}>
        //         <div>Status: <span>{this.props.channel.status}</span></div>
        //         <div>Last updated on: <span>{moment(this.props.channel.updated_at).format('MMMM Do YYYY')}</span></div>
        //     </div>
        // }
        // return <div className={this.context.styles['channel-item']}>
        //     <div>Status: <span>{this.props.channel.status}</span></div>
        //     <div>Last updated on: <span>{moment(this.props.channel.updated_at).format('MMMM Do YYYY')}</span></div>
        // </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
        // composite: state.composite,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateChannel
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);