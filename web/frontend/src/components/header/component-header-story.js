import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';
import { updateStory, changeTitle, selectPanel } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            is_title_editing: false,
            is_going_back_with_dirt: false
        };
    }
    componentDidMount() {

    }

    render() {
        let title, back, overlay;
        if (this.props.story.dirty) {
            back = <div onClick={() => {
                this.setState({
                    is_going_back_with_dirt: true,
                });
                this.props.selectPanel(null);
            }}>
                <img className={this.context.styles['back']} src={`${GLOBALS.API.STATIC_BASE}btn_header_back.png`} />
                Back
            </div>;
        } else {
            back = (<a href={GLOBALS.API.API_BASE + `/channel/${this.props.story.story.channel}`}>
                <img className={this.context.styles['back']} src={`${GLOBALS.API.STATIC_BASE}btn_header_back.png`} />
                Back
            </a>);
        }

        if (this.state.is_going_back_with_dirt) {
            overlay = (<div className={this.context.styles['overlay_middle']}>
                <div className={this.context.styles['container']}>
                    <div>
                        Do you want to save changes made to the story?
                    </div>
                    <div className={this.context.styles['actions']}>
                        <button className={this.context.styles['not_save']} onClick={() => {const canvas = document.getElementById("canvas");
                            const dataURL = canvas.toDataURL("image/png");
                            window.location = GLOBALS.API.API_BASE + `/channel/${this.props.story.story.channel}`;
                        }}>Don't Save</button>
                        <button className={this.context.styles['save']} onClick={() => {
                            this.props.selectPanel(null);
                            setTimeout(() => {
                                const canvas = document.getElementById("canvas");
                                const dataURL = canvas.toDataURL("image/png");
                                this.props.updateStory(dataURL).then(() => {
                                    window.location = GLOBALS.API.API_BASE + `/channel/${this.props.story.story.channel}`;
                                });
                            }, 500);

                        }}>Save</button>
                    </div>
                </div>
            </div>);
        }

        if (this.state.is_title_editing) {
            title = <input type="text" defaultValue={this.props.story.story.title} autoFocus onBlur={(event) => {
                this.setState({
                    is_title_editing: false,
                });
                this.props.changeTitle(event.target.value);
            }} />
        } else {
            title = <span onClick={() => {
                this.setState({
                    is_title_editing: true,
                });
            }}>{this.props.story.story.title}</span>
        }
        return <div className={this.context.styles['navigation']}>
            <button className={this.context.styles['left']}>
                {back}
            </button>
            <div className={this.context.styles['center']}>
                {title}
            </div>
            <button className={this.context.styles['right']} style={{
                color: this.props.story.dirty ? 'rgba(128,1,232,1)' : 'rgba(128,1,232,0.4)'
            }} onClick={() => {
                if (this.props.story.dirty) {
                    this.props.selectPanel(null);
                    setTimeout(() => {
                        this.props.selectPanel(null);
                        const canvas = document.getElementById("canvas");
                        const dataURL = canvas.toDataURL("image/png");
                        this.props.updateStory(dataURL);
                    }, 500);
                }
            }}>
                Save
            </button>
            {overlay}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        channel: state.channel
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateStory,
        changeTitle,
        selectPanel
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);