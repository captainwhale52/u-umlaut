import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';
import { createStory } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            is_title_editing: false,
            is_going_back_with_dirt: false
        };
    }
    componentDidMount() {

    }

    render() {
        return <div className={this.context.styles['navigation']}>
            <div className={this.context.styles['center']}>
                Settings
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        channel: state.channel,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);