import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from '../../globals/global-index';

import { setOverlayActivated } from './../../events/actions/action-overlay';
import { uploadStory } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        // FB.ui({
        //     method: 'share',
        //     href: 'https://developers.facebook.com/docs/'
        // }, function(response){});


    }

    // shouldComponentUpdate(nextProps, nextState) {
    //
    // }
    render() {
        return <div>
            <span>Share on Social Media</span>
            <hr />
            <a ref={(element) => { this.download_story = element; }}>
                <button onClick={(event) => {

                    const canvas = document.getElementById("canvas");
                    const dataURL = canvas.toDataURL("image/png");
                    this.download_story.href = dataURL;
                    this.download_story.download = 'story.png';
                }}>Download</button>
            </a>

            <button onClick={() => {
                const that = this;

                // const canvas = document.getElementById("canvas");
                // const dataURL = canvas.toDataURL("image/png");
                // // console.log(dataURL);
                // // document.write('<img src="'+dataURL+'"/>');
                // that.props.uploadStory(dataURL).then((success) => {
                //     console.log(success.image);
                // });

                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        console.log(response);
                        FB.login(function(){
                            const canvas = document.getElementById("canvas");
                            const dataURL = canvas.toDataURL("image/png");
                            // console.log(dataURL);
                            // document.write('<img src="'+dataURL+'"/>');
                            that.props.uploadStory(dataURL).then((success) => {
                                console.log(success.image);
                                FB.ui({
                                    method: 'share',
                                    href: success.image,
                                }, function(response){});
                            });
                        }, {scope: "publish_actions"});
                    } else {
                        FB.login();
                    }
                });

                // const body = 'My first post using facebook-node-sdk';
                // FB.api('me/feed', 'post', { message: body }, function (res) {
                //     if(!res || res.error) {
                //         console.log(!res ? 'error occurred' : res.error);
                //         return;
                //     }
                //     console.log('Post Id: ' + res.id);
                // });
            }}>Facebook</button>
            <button onClick={() => {
                this.props.setOverlayActivated(false);
            }}>Close</button>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        uploadStory,
        setOverlayActivated
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);