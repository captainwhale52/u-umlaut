import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectPhotoMode, setPanelPhoto, setPanelScale, changeTextSize, selectTextMode } from './../../events/actions/action-story';
import { fetchPhotos, postPhoto } from './../../events/actions/action-photo';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            selected_image: null,
        };
    }
    componentDidMount() {

    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.story.selected_text_index !== null) {
            // if (this.input_text) {
            //     this.input_text.value = this.props.story.story.texts[this.props.story.selected_text_index].text;
            // }
            // if (this.input_color) {
            //     this.input_color.value = this.props.story.story.texts[this.props.story.selected_text_index].color;
            // }
            if (this.input_font_size) {
                this.input_font_size.value = this.props.story.story.texts[this.props.story.selected_text_index].font_size;

            }
        }
    }

    render() {
        return <div className={this.context.styles['overlay_bottom']}>
            <div className={this.context.styles['left']}>
                <button onClick={() => {
                    this.props.selectTextMode(GLOBALS.CONTROLLER.TEXT.NONE);
                }}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_back.png`} />
                </button>
            </div>
            <div className={this.context.styles['right']}>
                <div className={this.context.styles['icon']}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom_out.png`} />
                </div>
                <div className={this.context.styles['controller']}>
                    <input id="input_font_size" type="range" min="10" max="200" step="1" ref={(element) => { this.input_font_size = element; }}
                        onChange={(event) => {
                            if (this.props.story.selected_text_index !== null) {
                                this.props.changeTextSize(event.target.value);
                            }
                        }
                    } />
                </div>
                <div className={this.context.styles['icon']}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom_in.png`} />
                </div>
            </div>

        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        photos: state.photos,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPhotoMode,
        fetchPhotos,
        setPanelPhoto,
        postPhoto,
        setPanelScale,
        changeTextSize,
        selectTextMode,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);