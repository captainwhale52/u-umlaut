import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { SketchPicker } from 'react-color';

import {
    selectPhotoMode, setPanelPhoto, changePanelTextColor, changePanelText, selectTextMode, removeText
} from './../../events/actions/action-story';
import { fetchPhotos, postPhoto } from './../../events/actions/action-photo';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            is_color_picker_on: false,
            color: '#000000'
        };
    }
    componentDidMount() {

    }

    handleChangeComplete = (color) => {
        console.log(color);
        console.log(this);
        this.props.changePanelTextColor(color.hex);
        this.setState({
            color
        });
    };

    render() {
        let color_picker;
        if (this.state.is_color_picker_on) {
            color_picker = <SketchPicker
                color={ this.state.color }
                onChangeComplete={ this.handleChangeComplete }
            />;
        }
        return <div className={this.context.styles['overlay_middle']}>
            <div className={this.context.styles['container']}>
                <div className={this.context.styles['field']}>
                    <input
                        id="input_new_text" type="text"
                        ref={(element) => { this.input_text = element; }}
                        placeholder="enter text..." onChange={(event) => {
                            this.props.changePanelText(event.target.value);
                        }
                    } onClick={() => {
                        this.setState({
                                is_color_picker_on: false,
                            });
                    }}/>
                </div>
                <div className={this.context.styles['controller']}>
                    <button onClick={() => {

                    }}>
                        <img src={`${GLOBALS.API.STATIC_BASE}btn_font.png`}  style={{
                            filter: 'grayscale(100%)',
                            opacity: 0.25
                        }}/>
                    </button>
                    <button>
                        <img src={`${GLOBALS.API.STATIC_BASE}btn_color_picker.png`} onClick={() => {
                            this.setState({
                                is_color_picker_on: !this.state.is_color_picker_on,
                            });
                        }}/>
                        {color_picker}
                        {/*<input id="input_color" type="color"*/}
                            {/*ref={(element) => { this.input_color = element; }} onChange={(event) => {*/}
                                {/*this.props.changePanelTextColor(event.target.value);*/}
                            {/*}*/}
                        {/*} />*/}
                    </button>

                    <button onClick={() => {
                        this.props.selectTextMode(GLOBALS.CONTROLLER.TEXT.NONE);
                    }}>
                        <img src={`${GLOBALS.API.STATIC_BASE}btn_checker.png`} />
                    </button>
                    <button onClick={() => {
                        this.props.removeText(this.props.story.selected_text_index);
                        this.props.selectTextMode(GLOBALS.CONTROLLER.TEXT.NONE);
                    }}>
                        <img src={`${GLOBALS.API.STATIC_BASE}btn_bin.png`} />
                    </button>

                </div>
            </div>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        photos: state.photos,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPhotoMode,
        fetchPhotos,
        setPanelPhoto,
        postPhoto,
        changePanelTextColor,
        changePanelText,
        selectTextMode,
        removeText
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);