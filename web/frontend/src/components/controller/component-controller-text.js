import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';
import { selectControllerMode, selectTextMode, setPanelPhoto, addText, removeText } from './../../events/actions/action-story';

import AddTextComponent from './component-text-add';
import ZoomTextComponent from './component-text-zoom';

import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let overlay;
        switch(this.props.story.text_mode) {
            case GLOBALS.CONTROLLER.TEXT.ADD: {
                overlay = <AddTextComponent />;
                break;
            }
            case GLOBALS.CONTROLLER.TEXT.ZOOM: {
                overlay = <ZoomTextComponent />;
                break;
            }
            default: {
                break;
            }
        }

        return <div className={this.context.styles['controller']}>
            <button onClick={() => {
                this.props.selectControllerMode(GLOBALS.CONTROLLER.MODE.NONE);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_close.png`} />
            </button>
            <button onClick={() => {
                this.props.selectTextMode(GLOBALS.CONTROLLER.TEXT.ADD);
                this.props.addText('');
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_add.png`} />
            </button>
            {/*<button onClick={() => {*/}
                {/*if (this.props.story.photo_mode !== GLOBALS.CONTROLLER.PHOTO.MOVE) {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.MOVE);*/}
                {/*} else {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.NONE);*/}
                {/*}*/}
            {/*}}>*/}
                {/*<img src={`${GLOBALS.API.STATIC_BASE}btn_move.png`} />*/}
            {/*</button>*/}
            <button onClick={() => {
                if (this.props.story.selected_text_index !== null) {
                    this.props.selectTextMode(GLOBALS.CONTROLLER.TEXT.ZOOM);
                }
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom.png`} />
            </button>
            <button onClick={() => {
                this.props.removeText(this.props.story.selected_text_index);
                // this.props.setPanelPhoto(null);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_delete.png`} />
            </button>
            {overlay}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectControllerMode,
        selectTextMode,
        setPanelPhoto,
        addText,
        removeText,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);