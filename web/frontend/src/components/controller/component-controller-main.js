import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';
import { selectControllerMode, selectPanel } from './../../events/actions/action-story';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let photo_action;
        if (this.props.story.selected_panel_index !== null) {
            photo_action = (<button onClick={() => {
                this.props.selectControllerMode(GLOBALS.CONTROLLER.MODE.PHOTO);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_controller_picture.png`} />
            </button>);
        } else {
            photo_action = (<button>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_controller_picture.png`} />
            </button>);
        }

        return <div className={this.context.styles['controller']}>
            {photo_action}
            <button onClick={() => {
                this.props.selectPanel(null);
                this.props.selectControllerMode(GLOBALS.CONTROLLER.MODE.TEXT);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_controller_text.png`} />
            </button>
            <button>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_controller_illustration.png`} style={{
                    filter: 'grayscale(100%)',
                    opacity: 0.25
                }}/>
            </button>
            <button>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_controller_layout.png`} style={{
                    filter: 'grayscale(100%)',
                    opacity: 0.25
                }} />
            </button>
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectControllerMode,
        selectPanel
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);