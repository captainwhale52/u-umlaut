import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { setOverlayActivated, setOverlayMode } from './../../events/actions/action-overlay';
import { selectControllerMode, selectPhotoMode, setPanelPhoto } from './../../events/actions/action-story';

import AddPhotoComponent from './component-photo-add';
import ZoomPhotoComponent from './component-photo-zoom';

import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let overlay;
        switch(this.props.story.photo_mode) {
            case GLOBALS.CONTROLLER.PHOTO.ADD: {
                overlay = <AddPhotoComponent />;
                break;
            }
            case GLOBALS.CONTROLLER.PHOTO.ZOOM: {
                overlay = <ZoomPhotoComponent />;
                break;
            }
            default: {
                break;
            }
        }

        return <div className={this.context.styles['controller']}>
            <button onClick={() => {
                this.props.selectControllerMode(GLOBALS.CONTROLLER.MODE.NONE);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_close.png`} />
            </button>
            <button onClick={() => {
                this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.ADD);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_add.png`} />
            </button>
            {/*<button onClick={() => {*/}
                {/*if (this.props.story.photo_mode !== GLOBALS.CONTROLLER.PHOTO.MOVE) {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.MOVE);*/}
                {/*} else {*/}
                    {/*this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.NONE);*/}
                {/*}*/}
            {/*}}>*/}
                {/*<img src={`${GLOBALS.API.STATIC_BASE}btn_move.png`} />*/}
            {/*</button>*/}
            <button onClick={() => {
                this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.ZOOM);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom.png`} />
            </button>
            <button onClick={() => {
                this.props.setPanelPhoto(null);
            }}>
                <img src={`${GLOBALS.API.STATIC_BASE}btn_delete.png`} />
            </button>
            {overlay}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectControllerMode,
        selectPhotoMode,
        setPanelPhoto,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);