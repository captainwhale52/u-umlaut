import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectPhotoMode, setPanelPhoto, setPanelScale } from './../../events/actions/action-story';
import { fetchPhotos, postPhoto } from './../../events/actions/action-photo';


import GLOBALS from './../../globals/global-index';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            selected_image: null,
        };
    }
    componentDidMount() {

    }

    render() {
        let default_scale_level = Math.log(this.props.story.story.photos[this.props.story.selected_panel_index].scale + 1) / Math.log(1.3);
        return <div className={this.context.styles['overlay_bottom']}>
            <div className={this.context.styles['left']}>
                <button onClick={() => {
                    this.props.selectPhotoMode(GLOBALS.CONTROLLER.PHOTO.NONE);
                }}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_back.png`} />
                </button>
            </div>
            <div className={this.context.styles['right']}>
                <div className={this.context.styles['icon']}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom_out.png`} />
                </div>
                <div className={this.context.styles['controller']}>
                    <input id="input_zoom_level" type="range" min="1" max="4" step="0.01" value={default_scale_level}
                        onChange={(event) => {
                            const scale = Math.round(Math.min(Math.max(Math.pow(1.3, event.target.value) - 1, 0.1), 2) * 100) / 100;
                            this.props.setPanelScale(scale);
                        }
                    } />
                </div>
                <div className={this.context.styles['icon']}>
                    <img src={`${GLOBALS.API.STATIC_BASE}btn_zoom_in.png`} />
                </div>
            </div>

        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        photos: state.photos,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        selectPhotoMode,
        fetchPhotos,
        setPanelPhoto,
        postPhoto,
        setPanelScale,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);