
import Immutable from 'seamless-immutable';

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  { fetchStories, createStory } from './../../events/actions/action-story';
import StoryItem from './component-story-item';

import GLOBALS from './../../globals/global-index';
import  { addNotification } from './../../events/actions/action-notification';



class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // is_first: true,
        };
    }
    componentWillMount() {

    }

    componentWillUpdate(nextProps) {
    }

    render() {
        if (this.props.story.stories.length) {
            const stories = this.props.story.stories.map((story, index) => {
                return <StoryItem key={`story-${story.pk}`} story={story} />;
            });
            return <div>
                {stories}
            </div>
        } else {
            return (
                <div className={this.context.styles['empty_story']}>
                    <button className={this.context.styles['add']} onClick={() => {
                        this.props.createStory('', this.props.channel.channel.pk);
                    }}>
                        <img src={`${GLOBALS.API.STATIC_BASE}add.png`} />
                    </button>
                </div>
            )
        }

    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        channel: state.channel,
        story: state.story
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchStories,
        createStory,
        addNotification
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);