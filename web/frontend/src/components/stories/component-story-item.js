import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { setCompositePhotoByIndex } from './../../events/actions/action-composite';
import * as moment from 'moment';

import GLOBALS from './../../globals/global-index';


import  { sendStory, deleteStory, selectStory } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let actions;
        if (this.props.user.user.pk === this.props.story.editor.pk) {
            actions = <div className={this.context.styles['actions']}>
                <button onClick={() => {
                    window.location.href = GLOBALS.API.API_BASE + `/story/${this.props.story.pk}`;
                }}>Edit</button>
                <button onClick={() => {
                    this.props.sendStory(this.props.story.pk);
                }}>Share</button>
            </div>
        } else {
            // actions = <div className={this.context.styles['actions']}>
            //     {/*<button onClick={() => {*/}
            //
            //     {/*}}>View</button>*/}
            // </div>
        }

        let thumbnail;
        if (this.props.story.thumbnail !== '' && this.props.story.thumbnail !== null) {
            thumbnail = <div className={this.context.styles['thumbnail']} style={{
                backgroundImage: `url(${this.props.story.thumbnail})`
            }} onClick={() => {
                this.props.selectStory(this.props.story.pk);
            }} />;
        } else {
            thumbnail = <div className={this.context.styles['thumbnail']} style={{
                backgroundImage: `url(${GLOBALS.API.STATIC_BASE}template.png`
            }} />;
        }

        return (<div className={this.context.styles['story_item']}>
            <div className={this.context.styles['container']}>
                <div className={this.context.styles['top']}>
                    <span className={this.context.styles['title']}>
                        {this.props.story.title}
                    </span>
                    <button className={this.context.styles['delete']} onClick={() => {
                        this.props.deleteStory(this.props.story.pk);
                    }}>
                        <img src={`${GLOBALS.API.STATIC_BASE}btn_bin.png`} />
                    </button>
                </div>
                <div>
                    {thumbnail}
                </div>
                <div>
                    <span className={this.context.styles['message']}>* Updated on {moment(this.props.updated_at).format(' Do MMM YY')} by {this.props.story.editor.first_name} {this.props.story.editor.last_name}</span>
                </div>
                {actions}
            </div>
            {/*<div>Current Editor: <span>{this.props.story.editor.first_name} {this.props.story.editor.last_name}</span></div>*/}
            {/*<div>Last updated: <span>{moment(this.props.updated_at).format('MMMM Do YYYY, h:mm:ss a')}</span></div>*/}

        </div>);
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
        // composite: state.composite,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        sendStory,
        deleteStory,
        selectStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);