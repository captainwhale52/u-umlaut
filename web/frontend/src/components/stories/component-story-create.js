import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { setCompositePhotoByIndex } from './../../events/actions/action-composite';
import * as moment from 'moment';

import GLOBALS from './../../globals/global-index';


import  { createStory } from './../../events/actions/action-story';


class Component extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        return (<div className={this.context.styles['story-item']}>
            <div>Create a new story</div>
            <hr/>
            <div>
                <label htmlFor="input_new_story_title">Title: </label>
                <input id="input_new_story_title" type="text" ref={(element) => { this.input_new_story_title = element; }} />
            </div>
            <div>
                <button onClick={() => {
                    this.props.createStory(this.input_new_story_title.value, this.props.channel.channel.pk);
                }}>Create</button>
            </div>

        </div>);
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
  styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user,
        story: state.story,
        channel: state.channel,
        // composite: state.composite,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);