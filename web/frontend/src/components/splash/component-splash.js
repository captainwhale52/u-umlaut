import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    shouldComponentUpdate(nextProps, nextState) {

    }
    render() {
        return (
            <div className={this.context.styles['logo-container']}>
                <img src={`https://dummyimage.com/1080x1920/000/fff`} />
            </div>
        );
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);