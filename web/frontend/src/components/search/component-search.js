import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  { searchUser } from './../../events/actions/action-user';
import  { createChannel } from './../../events/actions/action-channel';

class Component extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {

    }

    render() {
        let searched_result;
        if (this.props.user.searched_user) {
            searched_result = <div>
                <div>
                    SEARCH RESULT
                </div>
                <div>
                    <div>
                        {this.props.user.searched_user.username}
                    </div>
                    <div>
                        {this.props.user.searched_user.first_name} {this.props.user.searched_user.last_name}
                    </div>
                    <div>
                        {this.props.user.searched_user.email}
                    </div>
                </div>
                <button onClick={() => {
                    this.props.createChannel(this.props.user.searched_user.pk, 'LV');
                }}>Request</button>
                <hr/>
            </div>;
        }

        return <div>
            <div>
                SEARCH
            </div>
            <div>
                <label htmlFor="input_email_search">E-mail: </label>
                <input id="input_email_search" type="email" ref={(element) => { this.input_email_search = element; }} />
                <button onClick={() => {
                    this.props.searchUser(this.input_email_search.value);
                    // this.props.setCompositeZoomInByIndex(this.props.composite.editing_panel_index);
                }}>Search</button>
            </div>
            <hr/>
            {searched_result}
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        searchUser,
        createChannel
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);