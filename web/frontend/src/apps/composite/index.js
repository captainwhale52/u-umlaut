import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/composite.scss';

import LogoComponent from './../../components/logo/component-logo';
import UserComponent from './../../components/user/component-user';
import NavComponent from './../../components/nav/component-nav';
import CanvasComponent from './../../components/canvas/component-canvas';
import CanvasControlComponent from './../../components/canvas/component-canvas-control';



class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
      }

    getChildContext() {
        return { styles };
    }

    render() {
        return (
            <Provider store={store}>
                <div>
                    <LogoComponent />
                    <NavComponent />
                    <CanvasComponent />
                    <CanvasControlComponent />
                    <br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
