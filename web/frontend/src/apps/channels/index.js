import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/channels.scss';

import ChannelsApp from './app-channels';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    componentDidMount() {

    }
    render() {
        return (
            <Provider store={store}>
                <ChannelsApp />
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
