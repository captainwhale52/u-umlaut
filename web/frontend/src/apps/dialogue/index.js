import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/dialogue.scss';

import DialogueApp from './app-dialogue';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    componentDidMount() {

    }
    render() {
        return (
            <Provider store={store}>
                <DialogueApp />
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
