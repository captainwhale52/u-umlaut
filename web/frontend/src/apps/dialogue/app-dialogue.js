import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';

import Scroll from 'react-scroll';

let Link       = Scroll.Link;
let Element    = Scroll.Element;
let Events     = Scroll.Events;
let scroll     = Scroll.animateScroll;
// let scrollSpy  = Scroll.scrollSpy;

import SplashComponent from './../../components/splash/component-splash';
import LoginComponent from './../../components/user/component-login';
import RegisterComponent from './../../components/user/component-register';

import UserComponent from './../../components/user/component-user';
import AboutComponent from './../../components/about/component-about';

import  { fetchAuth } from './../../events/actions/action-user';


class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.fetchAuth();
    }

    render() {
        return <div>
            Dialogue
        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchAuth
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);