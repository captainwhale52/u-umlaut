import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/home.scss';

import SignupApp from './app-signup';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    componentDidMount() {

    }
    render() {
        return (
            <Provider store={store}>
                <SignupApp />
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
