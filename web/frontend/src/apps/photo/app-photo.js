import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  { addNotification } from './../../events/actions/action-notification';

import GLOBALS from './../../globals/global-index';

import Scroll from 'react-scroll';
import Immutable from 'seamless-immutable';

let Link       = Scroll.Link;
let Element    = Scroll.Element;
let Events     = Scroll.Events;
let scroll     = Scroll.animateScroll;
// let scrollSpy  = Scroll.scrollSpy;

// import SplashComponent from './../../components/splash/component-splash';
// import LoginComponent from './../../components/user/component-login';
// import RegisterComponent from './../../components/user/component-register';
//
// import UserComponent from './../../components/user/component-user';
// import AboutComponent from './../../components/about/component-about';

import LogoComponent from './../../components/logo/component-logo';
import NavComponent from './../../components/nav/component-nav';
import PhotoListComponent from './../../components/photo/component-photo-list';


import PhotoHeaderComponent from './../../components/header/component-header-photo';
import PhotoNavComponent from './../../components/nav/component-nav-photo';
import AddPhotoComponent from './../../components/controller/component-photo-add-2';


import  { fetchAuth } from './../../events/actions/action-user';
import  { fetchPhotos, postPhoto } from './../../events/actions/action-photo';
import  { fetchStories } from './../../events/actions/action-story';
import PopupNotificationComponent from './../../components/notification/component-notification-popup';

import  { fetchChannels } from './../../events/actions/action-channel';

class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            is_first: true,
        };
    }

    componentDidMount() {

    }

    componentWillUpdate(nextProps) {
        if (this.state.is_first) {
            if (nextProps.story.stories.length !== this.props.story.stories.length) {
                this.setState({
                    is_first: false,
                });
            }
        } else {
            nextProps.story.stories.filter((new_story, index) => {
                let is_new = true;
                this.props.story.stories.filter((old_story, index) => {
                    if (new_story.pk === old_story.pk && new_story.updated_at === old_story.updated_at) {
                        is_new = false;
                    }
                });
                if (is_new) {
                    const story = Immutable.asMutable(new_story, {deep: true});
                    story.action = 'STORY';
                    this.props.addNotification(story);
                }
            });
        }
    }

    componentWillMount() {
        this.props.fetchAuth().then(() => {
            this.props.fetchChannels().then(() => {
                this.setState({is_loaded: true});
            });

            this.props.fetchStories();
            setInterval(() => {
                this.props.fetchStories();
            }, 10000);
        });
    }

    render() {
        //
        // let user_component, login_component, register_component, buttons;
        //
        // if (this.props.user.fetched && this.props.user.error === null) {
        //     user_component = <UserComponent />;
        //     buttons = <div>
        //         <a href={'#'}><button>Upload Photo</button></a>
        //         <a href={GLOBALS.API.API_BASE + '/composite'}><button >Create Story</button></a>
        //     </div>
        // } else {
        //     login_component = <LoginComponent />;
        //     register_component = <RegisterComponent />;
        // }

        return (
            <div>
                <PhotoHeaderComponent />
                <div className={this.context.styles['body']} style={{
                    height: document.body.clientHeight - 58 - 50
                }} ref={(element) => { this.body = element; }}>
                    <AddPhotoComponent />
                </div>
                <PhotoNavComponent />
                <PopupNotificationComponent />
            </div>
        );

        // return <div>
        //     <LogoComponent />
        //     <NavComponent />
        //     <div>
        //         Photo Upload
        //     </div>
        //     <div>
        //         <label htmlFor="input_photo">Photo: </label>
        //         <input id="input_photo" type="file" accept="image/*" ref={(element) => { this.input_photo = element; }} />
        //     </div>
        //     <button onClick={() => {
        //         if (this.input_photo.files.length === 1) {
        //             this.props.postPhoto(this.input_photo.files[0]).then(() => {
        //                 this.props.fetchPhotos();
        //             });
        //         }
        //     }}>Submit</button>
        //     <PhotoListComponent />
        // </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user,
        story: state.story
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchAuth,
        fetchPhotos,
        postPhoto,
        fetchChannels,
        fetchStories,
        addNotification
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);