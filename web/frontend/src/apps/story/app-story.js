import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';

import Scroll from 'react-scroll';

let Link       = Scroll.Link;
let Element    = Scroll.Element;
let Events     = Scroll.Events;
let scroll     = Scroll.animateScroll;
// let scrollSpy  = Scroll.scrollSpy;

import SplashComponent from './../../components/splash/component-splash';
import LoginComponent from './../../components/user/component-login';
import RegisterComponent from './../../components/user/component-register';

import UserComponent from './../../components/user/component-user';
import AboutComponent from './../../components/about/component-about';

import  { fetchAuth } from './../../events/actions/action-user';
import  { fetchChannel } from './../../events/actions/action-channel';
import  { fetchStory } from './../../events/actions/action-story';

// import SearchComponent from './../../components/search/component-search';
import StoryListComponent from './../../components/stories/component-story-list';
import CanvasComponent from './../../components/canvas/component-canvas';
import CanvasControlComponent from './../../components/canvas/component-canvas-control';

import StoryHeaderComponent from './../../components/header/component-header-story';



class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            is_loaded: false
        };
    }

    componentWillMount() {
        this.props.fetchAuth().then(() => {
            this.props.fetchStory(window.u_umlaut.story_id).then(() => {
                this.props.fetchChannel(this.props.story.story.channel).then(() => {
                    this.setState({is_loaded: true});
                });
            })
        });
    }

    render() {
        if (this.state.is_loaded) {
            return (<div>
                <StoryHeaderComponent />
                {/*<div>STORY</div>*/}
                {/*<div>Title: <span>{this.props.story.story.title}</span></div>*/}
                {/*<hr/>*/}
                <CanvasComponent />
                <CanvasControlComponent />
            </div>)
        }
        return null;
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user,
        channel: state.channel,
        story: state.story,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchAuth,
        fetchChannel,
        fetchStory
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);