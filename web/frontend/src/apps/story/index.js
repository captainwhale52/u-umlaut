import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/story.scss';

import StoryApp from './app-story';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    componentDidMount() {

    }
    render() {
        return (
            <Provider store={store}>
                <StoryApp />
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
