import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../../globals/global-index';

import Scroll from 'react-scroll';

let Link       = Scroll.Link;
let Element    = Scroll.Element;
let Events     = Scroll.Events;
let scroll     = Scroll.animateScroll;
// let scrollSpy  = Scroll.scrollSpy;

import SplashComponent from './../../components/splash/component-splash';
import LoginComponent from './../../components/user/component-login';
import RegisterComponent from './../../components/user/component-register';

import UserComponent from './../../components/user/component-user';
import AboutComponent from './../../components/about/component-about';

import  { fetchAuth, postLogin } from './../../events/actions/action-user';


class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {

        };
    }

    componentDidMount() {
        this.props.fetchAuth();
    }

    render() {

        return (
          <div>
              <div className={this.context.styles['decoration']}>
                  <div className={this.context.styles['purple_background']} />
              </div>
              <div className={this.context.styles['title']}>
                  <div className={this.context.styles['main']}>
                      Lighten
                  </div>
                  <div className={this.context.styles['sub']}>
                      Your Story
                  </div>
              </div>
              <div className={this.context.styles['authentication']}>
                <div className={this.context.styles['container']}>
                    <div className={this.context.styles['logo']}>
                        <img src={`${GLOBALS.API.STATIC_BASE}icon.png`} />
                    </div>
                    <div className={this.context.styles['username']}>
                        <input id="login-username" type="text" autoCapitalize="none" ref={(element) => { this.input_username = element; }} placeholder="Username"/>
                    </div>
                    <hr/>
                    <div className={this.context.styles['password']}>
                        <input id="login-password" type="password" ref={(element) => { this.input_password = element; }} placeholder="Password"/>
                    </div>
                    <hr/>
                    <div className={this.context.styles['login']}>
                        <button onClick={() => {
                            this.props.postLogin(this.input_username.value, this.input_password.value);
                        }}>Login</button>
                    </div>
                    <div className={this.context.styles['reset_password']}>
                        Forget password?
                    </div>
                    <div className={this.context.styles['signup']}>
                        Signup
                    </div>
                </div>
              </div>

          </div>
        );

        let user_component, login_component, register_component, nav_component;

        if (this.props.user.fetched && this.props.user.error === null) {
            user_component = <UserComponent />;
            nav_component = <div>
                <a href={GLOBALS.API.API_BASE + '/channels/'}><button>Your stories</button></a>
                <a href={GLOBALS.API.API_BASE + '/photo/'}><button>Upload Photo</button></a>
            </div>
        } else {
            login_component = <LoginComponent />;
            register_component = <RegisterComponent />;
        }

        return <div>
            <Element name="splash">
                <Link to="about" smooth={true} offset={-8} duration={500}>
                    <SplashComponent />
                </Link>
                <hr />
            </Element>

            <Element name="about" className="element">
                <AboutComponent />
            </Element>

            {user_component}
            {login_component}
            {register_component}
            {nav_component}

        </div>
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        user: state.user,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchAuth,
        postLogin
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);