import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Immutable from 'seamless-immutable';
import  { addNotification } from './../../events/actions/action-notification';

import GLOBALS from './../../globals/global-index';

import Scroll from 'react-scroll';

let Link       = Scroll.Link;
let Element    = Scroll.Element;
let Events     = Scroll.Events;
let scroll     = Scroll.animateScroll;
// let scrollSpy  = Scroll.scrollSpy;

import SplashComponent from './../../components/splash/component-splash';
import LoginComponent from './../../components/user/component-login';
import RegisterComponent from './../../components/user/component-register';

import UserComponent from './../../components/user/component-user';
import AboutComponent from './../../components/about/component-about';

import  { fetchAuth } from './../../events/actions/action-user';
import  { fetchChannel } from './../../events/actions/action-channel';

// import SearchComponent from './../../components/search/component-search';
import StoryListComponent from './../../components/stories/component-story-list';
import NewStoryComponent from '../../components/stories/component-story-create';

import ChannelHeaderComponent from './../../components/header/component-header-channel';
import ChannelNavComponent from './../../components/nav/component-nav-channel';

import PopupNotificationComponent from './../../components/notification/component-notification-popup';

import  { fetchStories } from './../../events/actions/action-story';

import StoryPhotoComponent from './../../components/photo/component-story-photo';



class Component extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            is_loaded: false,
            is_first: true,
        };
    }

    componentDidMount() {
    }

    componentWillUpdate(nextProps) {
        if (this.state.is_first) {
            if (nextProps.story.stories.length !== this.props.story.stories.length) {
                this.setState({
                    is_first: false,
                });
            }
        } else {
            nextProps.story.stories.filter((new_story, index) => {
                let is_new = true;
                this.props.story.stories.filter((old_story, index) => {
                    if (new_story.pk === old_story.pk && new_story.updated_at === old_story.updated_at) {
                        is_new = false;
                    }
                });
                if (is_new) {
                    const story = Immutable.asMutable(new_story, {deep: true});
                    story.action = 'STORY';
                    this.props.addNotification(story);
                }
            });
        }
    }

    componentWillMount() {
        this.props.fetchAuth().then(() => {
            this.props.fetchChannel(window.u_umlaut.channel_id).then(() => {
                this.setState({is_loaded: true});
            });
            this.props.fetchStories();
            setInterval(() => {
                this.props.fetchStories();
            }, 10000);
        });
    }

    render() {
        let overlay;
        if (this.props.story.story) {
            overlay = <StoryPhotoComponent />
        }
        return (
            <div>
                <ChannelHeaderComponent />
                <div className={this.context.styles['body']} style={{
                    height: document.body.clientHeight - 58 - 50
                }} ref={(element) => { this.body = element; }}>
                    <StoryListComponent />
                </div>
                <ChannelNavComponent />
                <PopupNotificationComponent />
                {overlay}
            </div>
        );
        // if (this.state.is_loaded) {
        //     const recipient = this.props.user.user.pk === this.props.channel.channel.recipient.pk ? 'You' : `${this.props.channel.channel.recipient.first_name} ${this.props.channel.channel.recipient.last_name}`;
        //
        //     const requester = this.props.user.user.pk === this.props.channel.channel.requester.pk ? 'You' : `${this.props.channel.channel.requester.first_name} ${this.props.channel.channel.requester.last_name}`;
        //
        //     return (<div>
        //         STORIES
        //         <hr/>
        //         <div>
        //             Here are stories between <span>{requester} & {recipient}</span>
        //         </div>
        //         <StoryListComponent />
        //         <NewStoryComponent />
        //     </div>)
        // }
        // return null;
    }
}

Component.defaultProps = {};

Component.propTypes = {};

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        story: state.story,
        user: state.user,
        channel: state.channel
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchAuth,
        fetchChannel,
        fetchStories,
        addNotification
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);