import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import store from '../../events/stores/store-index';
import styles from '../../styles/apps/splash.scss';

import LogoComponent from './../../components/logo/component-logo';
import UserComponent from './../../components/user/component-user';
import NavComponent from './../../components/nav/component-nav';

import GLOBALS from './../../globals/global-index';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    render() {
        return (
            <Provider store={store}>
                <div>
                    <LogoComponent />
                </div>
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
