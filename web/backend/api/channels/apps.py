from django.apps import AppConfig


class ChannelConfig(AppConfig):
    name = 'backend.api.channels'
