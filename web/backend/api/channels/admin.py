from django.contrib import admin

from backend.api.channels.models import Channel

# Register your models here.
admin.site.register(Channel)