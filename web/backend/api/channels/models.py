import uuid
from django.db import models
from django.contrib.auth import get_user_model
from django_resized import ResizedImageField

User = get_user_model()


class Channel(models.Model):
    LOVER = "LV"
    FRIEND = "FD"
    FAMILY = "FM"
    COWORKER = "CW"
    CHANNEL_TYPE_CHOICES = (
        (LOVER, "Lover"),
        (FRIEND, "Friend"),
        (FAMILY, "Family"),
        (COWORKER, "Coworker")
    )
    WAITING = "WT"
    ACCEPTED = "AP"
    DECLINED = "DC"
    INACTIVE = "IN"
    CHANNEL_STATUS_CHOICES = (
        (WAITING, "Waiting"),
        (ACCEPTED, "Accepted"),
        (DECLINED, "Declined"),
        (INACTIVE, "Inactive")
    )
    requester = models.ForeignKey(User, related_name='requester', null=True)
    recipient = models.ForeignKey(User, related_name='recipient', null=True)
    type = models.CharField(
        max_length=2,
        choices=CHANNEL_TYPE_CHOICES,
        default=LOVER,
    )
    status = models.CharField(
        max_length=2,
        choices=CHANNEL_STATUS_CHOICES,
        default=WAITING,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated_at',)

