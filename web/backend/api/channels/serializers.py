from rest_framework import serializers

from backend.api.channels.models import Channel  # Import our UploadedImage model
from django.db import models
from django.contrib.auth import get_user_model

from backend.api.accounts.serializers import UserDetailSerializer
from django.db.models import Q


# from datetime import datetime
#
# from django.utils import timezone

User = get_user_model()


class ChannelListSerializer(serializers.ModelSerializer):
    requester = UserDetailSerializer()
    recipient = UserDetailSerializer()

    class Meta:
        model = Channel
        fields = (
            'pk',
            'requester',
            'recipient',
            'type',
            'status',
            'created_at',
            'updated_at'
        )
        depth = 1


class ChannelUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        read_only_fields = ('requester', 'recipient')
        fields = [
            'pk',
            'requester',
            'recipient',
            'type',
            'status',
            'created_at',
            'updated_at'
        ]

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance


class ChannelCreateSerializer(serializers.ModelSerializer):
    requester = UserDetailSerializer(read_only=True)
    recipient = UserDetailSerializer(read_only=True)

    class Meta:
        model = Channel
        fields = (
            'pk',
            'requester',
            'recipient',
            'type',
            'status',
            'created_at',
            'updated_at'
        )
        depth = 1

    def validate(self, data):
        data['requester'] = self.context['request'].user

        recipient_obj = None
        user = User.objects.filter(
            Q(pk=self.context['request'].data['recipient'])
        ).distinct()

        if user.exists() and user.count() == 1:
            recipient_obj = user.first()
        else:
            raise serializers.ValidationError('Recipient is a required field.')

        data['recipient'] = recipient_obj

        channel = Channel.objects.filter(
            Q(requester=data['requester'].pk, recipient=data['recipient'].pk) |
            Q(requester=data['recipient'].pk, recipient=data['requester'].pk)
        ).distinct()

        if channel.exists() and channel.count() == 1:
            raise serializers.ValidationError('Channel already exists.')

        return data


class ChannelDetailSerializer(serializers.ModelSerializer):
    requester = UserDetailSerializer()
    recipient = UserDetailSerializer()

    class Meta:
        model = Channel
        fields = (
            'pk',
            'requester',
            'recipient',
            'type',
            'status',
            'created_at',
            'updated_at'
        )
        depth = 1
