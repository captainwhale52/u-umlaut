from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
import django_filters.rest_framework
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated


# Create your views here.
# @api_view(['GET'])
# def index(request):
#     meta_response = {
#         'version': '0.1.0'
#     }
#     return Response(meta_response)

from rest_framework.generics import ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView, CreateAPIView, RetrieveUpdateAPIView

from backend.api.channels.models import Channel
from backend.api.channels.serializers import (
    ChannelListSerializer, ChannelUpdateSerializer, ChannelCreateSerializer, ChannelDetailSerializer
)


class ChannelListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Channel.objects.all()
    serializer_class = ChannelListSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Channel.objects.all()

        return Channel.objects.filter(
            Q(requester=self.request.user) |
            Q(recipient=self.request.user)
        ).distinct()


class ChannelUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Channel.objects.all()
    serializer_class = ChannelUpdateSerializer

    def perform_update(self, serializer):
        serializer.save(status=self.request.data['status'])


class ChannelCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Channel.objects.all()
    serializer_class = ChannelCreateSerializer

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)



class ChannelDetailAPIView(RetrieveAPIView):
    queryset = Channel.objects.all()
    serializer_class = ChannelDetailSerializer
