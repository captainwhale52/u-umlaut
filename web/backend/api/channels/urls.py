from backend.api.photos import views
from django.conf.urls import include, url
from rest_framework import routers

# from backend.api.photos.viewsets import PhotoViewSet
#
# router = routers.DefaultRouter()
# router.register('images', PhotoViewSet, 'images')

from backend.api.channels.views import (
    ChannelListAPIView,
    ChannelUpdateAPIView,
    ChannelCreateAPIView,
    ChannelDetailAPIView
)


urlpatterns = [
    # url(r'^', include(router.urls)),
    url(r'^$', ChannelListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', ChannelDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', ChannelUpdateAPIView.as_view(), name='update'),
    url(r'^create/$', ChannelCreateAPIView.as_view(), name='create'),
]

