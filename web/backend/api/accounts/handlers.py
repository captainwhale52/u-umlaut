from backend.api.accounts.serializers import UserCreateSerializer, UserLoginSerializer


def jwt_response_payload_handler(token, user=None, request=None):
    # return {
    #     'token': token,
    #     'user': UserLoginSerializer(user, context={'request': request}).data
    # }

    # if not request.user.is_authenticated:
    #     return {
    #         'user': None
    #     }
    return {
        'user': UserLoginSerializer(user, context={'request': request}).data
    }