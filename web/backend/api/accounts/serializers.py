from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()


class UserCreateSerializer(serializers.ModelSerializer):
    token = serializers.CharField(allow_blank=True, read_only=True)
    email = serializers.EmailField(label = 'Email Address')
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
            'token'
        ]
        extra_kwargs = {'password':
            {
                'write_only': True
            }
        }

    def validate_email(self, value):
        email = value
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise serializers.ValidationError("A user with that email address already exists")
        return value

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        password = validated_data['password']
        user_obj = User(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name
        )
        user_obj.set_password(password)
        user_obj.save()

        user_payload = jwt_payload_handler(user_obj)
        user_token = jwt_encode_handler(user_payload)

        validated_data['token'] = user_token

        return validated_data


class UserLoginSerializer(serializers.ModelSerializer):
    token = serializers.CharField(allow_blank=True, read_only=True)
    username = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField(label='Email Address', required=False, allow_blank=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    class Meta:
        model = User
        fields = [
            'pk',
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
            'token'
        ]
        extra_kwargs = {'password':
            {
                'write_only': True
            }
        }

    def validate(self, data):
        user_obj = None
        email = data.get('email', None)
        username = data.get('username', None)
        password = data['password']
        if not email and not username:
            raise serializers.ValidationError('A username or email is required to login.')

        user = User.objects.filter(
            Q(email=email) |
            Q(username=username)
        ).distinct()

        # user = user.exclude(email__isnull=True).exlcude(email__isaxact='')  # In case there are some accounts without an email address.
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise serializers.ValidationError('This username / email is not valid.')

        if user_obj:
            if not user_obj.check_password(password):
                raise serializers.ValidationError('Incorrect credentials.')

        user_payload = jwt_payload_handler(user_obj)
        user_token = jwt_encode_handler(user_payload)

        data['token'] = user_token
        data['email'] = user_obj.email
        data['first_name'] = user_obj.first_name
        data['last_name'] = user_obj.last_name

        return data


class UserSearchSerializer(serializers.ModelSerializer):
    # token = serializers.CharField(allow_blank=True, read_only=True)
    email = serializers.EmailField(label='Email Address', required=False, allow_blank=True)
    username = serializers.CharField(allow_blank=True, read_only=True)

    class Meta:
        model = User
        fields = [
            'pk',
            'username',
            'email',
            'first_name',
            'last_name',
        ]
        # extra_kwargs = {'password':
        #     {
        #         'write_only': True
        #     }
        # }

    def validate(self, data):
        user_obj = None
        email = data.get('email', None)
        # username = data.get('username', None)
        # password = data['password']
        if not email:
            raise serializers.ValidationError('A email is required to search.')

        user = User.objects.filter(
            Q(email=email)
        ).distinct()

        # user = user.exclude(email__isnull=True).exlcude(email__isaxact='')  # In case there are some accounts without an email address.
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise serializers.ValidationError('This email doesn\' exist.')

        user_payload = jwt_payload_handler(user_obj)
        user_token = jwt_encode_handler(user_payload)

        # data['token'] = user_token
        data['pk'] = user_obj.pk
        data['email'] = user_obj.email
        data['first_name'] = user_obj.first_name
        data['last_name'] = user_obj.last_name
        data['username'] = user_obj.username

        return data


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'pk',
            'username',
            'email',
            'first_name',
            'last_name',
        )