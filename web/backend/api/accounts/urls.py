
from django.conf.urls import include, url
from backend.api.accounts import views

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token


urlpatterns = [
    url(r'^register/', views.UserCreateAPIView.as_view(), name='register'),
    url(r'^login/', views.UserLoginAPIView.as_view(), name='login'),

    url(r'^auth/', obtain_jwt_token),
    url(r'^refresh/', refresh_jwt_token),
    url(r'^verify/', verify_jwt_token),

    url(r'search/', views.UserSearchAPIView.as_view(), name='search')
    # url(r'^logout/', views.UserLogoutAPIView.as_view()),
]
