from backend.api.accounts.serializers import UserCreateSerializer, UserLoginSerializer, UserSearchSerializer
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.authtoken.models import Token

User = get_user_model()


class UserCreateAPIView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()


class UserLoginAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserSearchAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSearchSerializer

    def get(self, request, format=None):
        data = request.query_params
        serializer = UserSearchSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)



# class UserLogoutAPIView(APIView):
#     permission_classes = (AllowAny,)
#     queryset = User.objects.all()
#
#     def get(self, request, format=None):
#         # # simply delete the token to force a login
#         # request.user.auth_token.delete()
#         # return Response(status=HTTP_200_OK)
#         request.session.flush()
#         # Token.objects.get(user_id=request.user.id).delete()
#         return Response(status=HTTP_200_OK)
#         # return HttpResponseRedirect('/')
