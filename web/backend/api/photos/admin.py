from django.contrib import admin

from backend.api.photos.models import Photo

# Register your models here.
admin.site.register(Photo)