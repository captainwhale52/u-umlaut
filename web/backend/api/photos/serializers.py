from rest_framework import serializers

from backend.api.photos.models import Photo  # Import our UploadedImage model
from django.db import models
from django.contrib.auth import get_user_model

from datetime import datetime

from django.utils import timezone

from PIL import Image
from PIL.ExifTags import TAGS

User = get_user_model()


class PhotoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = (
            'pk',
            'image',
            'user',
            'taken_at',
        )


class PhotoDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = (
            'pk',
            'image',
            'user',
            'taken_at',
            'updated_at',
            'created_at',
        )


class PhotoUpdateSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(read_only=True)

    class Meta:
        model = Photo
        fields = (
            'pk',
            'image',
            'user',
            'taken_at',
            'updated_at',
            'created_at',
        )


class PhotoDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = (
            'pk',
        )


class PhotoCreateSerializer(serializers.ModelSerializer):
    taken_at = serializers.DateTimeField(default=timezone.now)

    class Meta:
        model = Photo
        fields = (
            'image',
            'taken_at',
        )

    def validate(self, data):
        image = Image.open(data.get('image', None))
        #     width, height = image.size
        #     new_width = width
        #     new_height = height
        #     if width > height:
        #         new_width = 1920
        #         scale = new_width / width
        #         new_height = int(height * scale)
        #     else:
        #         new_height = 1920
        #         scale = new_height / height
        #         new_width = int(width * scale)

        #################################### TODO-Karl Doesn't work for iOS ####################################
        # if image:
            # info = image._getexif()
            # # new_image = image.thumbnail((1920, 1920), Image.ANTIALIAS)
            # # new_image = image.resize((new_width, new_height), Image.ANTIALIAS)
            # if info:
            #     try:
            #         taken_at = datetime.strptime(image._getexif()[36867], '%Y:%m:%d %H:%M:%S')
            #         if taken_at:
            #             data['taken_at'] = taken_at
            #     except:
            #         data['taken_at'] = datetime.now()
        #################################### TODO-Karl Doesn't work for iOS ####################################
        
        data['taken_at'] = datetime.now()
        return data

    # def validate_image(self, value):
    #     image = Image.open(value)
    #     info = image._getexif()
    #     dateTime = datetime.strptime(image._getexif()[36867], '%Y:%m:%d %H:%M:%S')
    #
    #     # ret = {}
    #     # i = Image.open(fn)
    #     # info = i._getexif()
    #     # user_qs = User.objects.filter(email=email)
    #     # if user_qs.exists():
    #     #     raise serializers.ValidationError("A user with that email address already exists")
    #     return value