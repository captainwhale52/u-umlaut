# from backend.api.photos.serializers import PhotoSerializer  # import our serializer
# from rest_framework import viewsets
# from rest_framework.decorators import list_route
# from rest_framework.response import Response
#
# from backend.api.photos.models import Photo  # import our model
#
#
# class PhotoViewSet(viewsets.ModelViewSet):
#     queryset = Photo.objects.all()
#     serializer_class = PhotoSerializer
#
#     @list_route()
#     def recent_photos(self, request):
#         recent_photos = Photo.objects.all().order_by('-id')[:5]
#
#         serializer = self.get_serializer(recent_photos, many=True)
#         return Response(serializer.data)