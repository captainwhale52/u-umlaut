from backend.api.photos import views
from django.conf.urls import include, url
from rest_framework import routers

# from backend.api.photos.viewsets import PhotoViewSet
#
# router = routers.DefaultRouter()
# router.register('images', PhotoViewSet, 'images')

from backend.api.photos.views import (
    PhotoListAPIView, PhotoDetailAPIView, PhotoUpdateAPIView, PhotoDeleteAPIView, PhotoCreateAPIView
)


urlpatterns = [
    # url(r'^', include(router.urls)),
    url(r'^$', PhotoListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', PhotoDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', PhotoUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/$', PhotoDeleteAPIView.as_view(), name='delete'),
    url(r'^create/$', PhotoCreateAPIView.as_view(), name='create'),
]

