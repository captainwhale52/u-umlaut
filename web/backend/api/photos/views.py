from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
import django_filters.rest_framework


# Create your views here.
# @api_view(['GET'])
# def index(request):
#     meta_response = {
#         'version': '0.1.0'
#     }
#     return Response(meta_response)


from rest_framework.generics import ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView, CreateAPIView, RetrieveUpdateAPIView

from backend.api.photos.models import Photo
from backend.api.photos.serializers import (
    PhotoListSerializer, PhotoDetailSerializer, PhotoDeleteSerializer, PhotoUpdateSerializer, PhotoCreateSerializer
)


class PhotoListAPIView(ListAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoListSerializer

    def get_queryset(self):
        return Photo.objects.all() # TODO-Karl: Return all images for user-testing purpose.
        # if self.request.user.is_superuser:
        #     return Photo.objects.all()
        # return Photo.objects.filter(user=self.request.user)


class PhotoDetailAPIView(RetrieveAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoDetailSerializer


class PhotoUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoUpdateSerializer

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class PhotoDeleteAPIView(DestroyAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoDeleteSerializer


class PhotoCreateAPIView(CreateAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
