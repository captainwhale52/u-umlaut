import uuid
from django.db import models
from django.contrib.auth import get_user_model
from django_resized import ResizedImageField

User = get_user_model()


# Create your models here.
def scramble_uploaded_filename(instance, filename):
    extension = filename.split(".")[-1]
    return "{}.{}".format(uuid.uuid4(), extension)


class Photo(models.Model):
    user = models.ForeignKey(User, related_name='uploader', null=True)
    image = ResizedImageField("image", upload_to=scramble_uploaded_filename) # stores the filename of an uploaded image
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    taken_at = models.DateTimeField()

    class Meta:
        ordering = ('-taken_at',)
