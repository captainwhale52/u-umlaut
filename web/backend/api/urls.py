from backend.view import views
from django.conf.urls import include, url
from rest_framework import routers


urlpatterns = [
    url(r'^photos/', include('backend.api.photos.urls', namespace="photos")),
    url(r'^stories/', include('backend.api.stories.urls', namespace="stories")),
    url(r'^users/', include('backend.api.accounts.urls', namespace="users")),
    url(r'^channels/', include('backend.api.channels.urls', namespace="channels")),
]
