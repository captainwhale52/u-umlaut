from django.db.models import Q
from rest_framework.permissions import IsAuthenticated

from rest_framework.generics import ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework import status


from backend.api.stories.models import Story
from backend.api.stories.serializers import (
    StoryListSerializer,
    StoryDetailSerializer,
    StoryUpdateSerializer,
    StorySendSerializer,
    StoryCreateSerializer,
    StoryDeleteSerializer
)


class StoryListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Story.objects.all()
    serializer_class = StoryListSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Story.objects.all()

        return Story.objects.filter(
            Q(creators__in=[self.request.user])
        ).distinct()


class StoryDetailAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Story.objects.all()
    serializer_class = StoryDetailSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Story.objects.all()

        return Story.objects.filter(
            Q(creators__in=[self.request.user])
        ).distinct()


class StoryUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Story.objects.all()
    serializer_class = StoryUpdateSerializer
    #
    # def perform_update(self, serializer):
    #     serializer.save(status=self.request.data['status'])


class StorySendAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Story.objects.all()
    serializer_class = StorySendSerializer

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class StoryCreateAPIView(CreateAPIView):
    queryset = Story.objects.all()
    serializer_class = StoryCreateSerializer

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)


class StoryDeleteAPIView(DestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Story.objects.all()
    serializer_class = StoryDeleteSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        pk = instance.pk
        self.perform_destroy(instance)
        data = StoryDeleteSerializer(instance=instance).data
        data['pk'] = pk
        return Response(status=status.HTTP_200_OK, data=data)

    def perform_destroy(self, instance):
        instance.delete()