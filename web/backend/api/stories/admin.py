from django.contrib import admin

from backend.api.stories.models import Story

# Register your models here.
admin.site.register(Story)