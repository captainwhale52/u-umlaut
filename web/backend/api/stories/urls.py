from backend.api.stories import views
from django.conf.urls import include, url
from rest_framework import routers

# from backend.api.stories.viewsets import StoryViewSet
#
# router = routers.DefaultRouter()
# router.register('images', StoryViewSet, 'images')

from backend.api.stories.views import (
    StoryListAPIView,
    StoryDetailAPIView,
    StoryUpdateAPIView,
    StorySendAPIView,
    StoryCreateAPIView,
    StoryDeleteAPIView
)

urlpatterns = [
    url(r'^$', StoryListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', StoryDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/$', StoryUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/send/$', StorySendAPIView.as_view(), name='send'),
    url(r'^create/$', StoryCreateAPIView.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/delete/$', StoryDeleteAPIView.as_view(), name='delete'),
]
