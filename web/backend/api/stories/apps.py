from django.apps import AppConfig


class StoryConfig(AppConfig):
    name = 'backend.api.stories'
