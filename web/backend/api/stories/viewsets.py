from backend.api.stories.serializers import StorySerializer  # import our serializer
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from backend.api.stories.models import Story  # import our model


class StoryViewSet(viewsets.ModelViewSet):
    queryset = Story.objects.all()
    serializer_class = StorySerializer

    @list_route()
    def recent_stories(self, request):
        recent_stories = Story.objects.all().order_by('-id')[:5]

        serializer = self.get_serializer(recent_stories, many=True)
        return Response(serializer.data)