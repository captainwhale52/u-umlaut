import uuid
from django.db import models
from backend.api.photos.models import Photo
from backend.api.channels.models import Channel
from backend.api.accounts.serializers import UserDetailSerializer
from django.contrib.auth import get_user_model
import json
from django_resized import ResizedImageField


User = get_user_model()

# Create your models here.
def scramble_uploaded_filename(instance, filename):

    extension = filename.split(".")[-1]
    return "{}.{}".format(uuid.uuid4(), 'png')


class Story(models.Model):
    title = models.CharField(default="", max_length=128)
    creators = models.ManyToManyField(User)
    editor = models.ForeignKey(User, related_name='editor', null=True)
    channel = models.ForeignKey(Channel, related_name='channel', null=True)

    # with open('templates/template_1_photos.json') as data_file:
    #     photos_default = json.load(data_file)

    photos = models.TextField(default=open('backend/api/stories/templates/template_1_photos.json').read())
    texts = models.TextField(default='[]')
    icons = models.TextField(default='[]')
    panels = models.TextField(default=open('backend/api/stories/templates/template_1_panels.json').read())
    properties = models.TextField(default=open('backend/api/stories/templates/template_1_properties.json').read())

    thumbnail = ResizedImageField("thumbnail", upload_to=scramble_uploaded_filename, blank=True, null=True)  # stores the filename of an uploaded image
    # thumbnail = models.ImageField("thumbnail", upload_to=scramble_uploaded_filename, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated_at',)

