from rest_framework import serializers

from backend.api.stories.models import Story
from backend.api.channels.models import Channel
from backend.api.accounts.serializers import UserDetailSerializer
from django.contrib.auth import get_user_model
from django.db.models import Q

User = get_user_model()

from rest_framework import serializers

class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension

class StorySerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = Story
        fields = ('pk', 'image', ) # only serialize the primary key and the image field


class StoryListSerializer(serializers.ModelSerializer):
    editor = UserDetailSerializer()

    class Meta:
        model = Story
        fields = (
            'pk',
            'title',
            # 'creators', TODO Remove password from Many-To-Many field, 'creators' field & return as response data.
            'editor',
            'channel',
            'photos',
            'texts',
            'icons',
            'panels',
            'properties',
            'updated_at',
            'created_at',
            'thumbnail'
        )
        # depth = 1


class StoryDetailSerializer(serializers.ModelSerializer):
    editor = UserDetailSerializer()

    class Meta:
        model = Story
        fields = (
            'pk',
            'title',
            'creators',  # TODO Remove password from Many-To-Many field, 'creators' field & return as response data.
            'editor',
            'channel',
            'photos',
            'texts',
            'icons',
            'panels',
            'properties',
            'updated_at',
            'created_at',
            'thumbnail'
        )
        # depth = 1


class StoryUpdateSerializer(serializers.ModelSerializer):
    editor = UserDetailSerializer(read_only=True)

    class Meta:
        model = Story
        read_only_fields = ('creators', 'editor')
        fields = (
            'pk',
            'title',
            'creators',  # TODO Remove password from Many-To-Many field, 'creators' field & return as response data.
            'editor',
            'channel',
            'photos',
            'texts',
            'icons',
            'panels',
            'properties',
            'updated_at',
            'created_at',
            'thumbnail'
        )
        # depth = 1

    # def update(self, instance, validated_data):
    #     instance.status = validated_data.get('status', instance.status)
    #     instance.save()
    #     return instance


class StorySendSerializer(serializers.ModelSerializer):
    # creators = UserDetailSerializer(read_only=True, many=True)
    editor = UserDetailSerializer(read_only=True)

    class Meta:
        model = Story
        read_only_fields = ('creators', 'editor', 'thumbnail')
        fields = (
            'pk',
            'title',
            'creators',  # TODO Remove password from Many-To-Many field, 'creators' field & return as response data.
            'editor',
            'channel',
            'photos',
            'texts',
            'icons',
            'panels',
            'properties',
            'updated_at',
            'created_at',
            'thumbnail'
        )
        # depth = 1

    def update(self, instance, validated_data):
        editor_id = instance.editor_id
        creators = UserDetailSerializer(instance.creators, many=True).data
        for creator in creators:
            if editor_id is not creator['pk']:
                editor_id = creator['pk']
                break

        editor_obj = None
        user = User.objects.filter(
            Q(pk=editor_id)
        ).distinct()

        if user.exists() and user.count() == 1:
            editor_obj = user.first()
        else:
            raise serializers.ValidationError("Editor doesn't exist.")

        instance.editor = editor_obj
        instance.save()
        return instance

    # def validate(self, data):
    #     return data


class StoryCreateSerializer(serializers.ModelSerializer):
    creators = UserDetailSerializer(read_only=True, many=True)
    editor = UserDetailSerializer(read_only=True)

    class Meta:
        model = Story
        read_only_fields = ('creators', 'editor')
        fields = (
            'pk',
            'title',
            'creators',  # TODO Remove password from Many-To-Many field, 'creators' field & return as response data.
            'editor',
            'channel',
            'photos',
            'texts',
            'icons',
            'panels',
            'properties',
            'updated_at',
            'created_at',
        )
        # depth = 1

    def create(self, validated_data):
        # self.context['request'].user
        # username = validated_data['username']
        # email = validated_data['email']
        # first_name = validated_data['first_name']
        # last_name = validated_data['last_name']
        # password = validated_data['password']
        # user_obj = User(
        #     username=username,
        #     email=email,
        #     first_name=first_name,
        #     last_name=last_name
        # )
        # user_obj.set_password(password)
        # user_obj.save()
        #
        # user_payload = jwt_payload_handler(user_obj)
        # user_token = jwt_encode_handler(user_payload)
        #
        # validated_data['token'] = user_token

        channel_obj = None
        channel = Channel.objects.filter(
            Q(pk=validated_data['channel'].pk)
        ).distinct()

        # user = user.exclude(email__isnull=True).exlcude(email__isaxact='')  # In case there are some accounts without an email address.
        if channel.exists() and channel.count() == 1:
            channel_obj = channel.first()
        else:
            raise serializers.ValidationError("The channel doesn't exist.")



        story_obj = Story(
            editor=self.context['request'].user,
            channel=channel_obj,
            title=validated_data['title'],
            # creators=[channel_obj.requester.pk, channel_obj.recipient.pk]
        )

        story_obj.save()
        story_obj.creators.add(channel_obj.requester, channel_obj.recipient)

        validated_data['pk'] = story_obj.pk
        validated_data['editor'] = self.context['request'].user
        validated_data['photos'] = story_obj.photos
        validated_data['texts'] = story_obj.texts
        validated_data['icons'] = story_obj.icons
        validated_data['panels'] = story_obj.panels
        validated_data['properties'] = story_obj.properties
        validated_data['updated_at'] = story_obj.updated_at
        validated_data['created_at'] = story_obj.created_at

        return validated_data

    # def update(self, instance, validated_data):
    #     editor_id = instance.editor_id
    #     creators = UserDetailSerializer(instance.creators, many=True).data
    #     for creator in creators:
    #         if editor_id is not creator['pk']:
    #             editor_id = creator['pk']
    #             break
    #
    #     editor_obj = None
    #     user = User.objects.filter(
    #         Q(pk=editor_id)
    #     ).distinct()
    #
    #     if user.exists() and user.count() == 1:
    #         editor_obj = user.first()
    #     else:
    #         raise serializers.ValidationError("Editor doesn't exist.")
    #
    #     instance.editor = editor_obj
    #     instance.save()
    #     return instance

    # def validate(self, data):
    #     return data


class StoryDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = (
            'pk',
        )

