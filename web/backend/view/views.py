from django.shortcuts import render


# Create your views here.
def splash(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'splash/index.html')


def composite(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'composite/index.html')


def home(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'home/index.html')


def photo(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'photo/index.html')


def dialogue(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'dialogue/index.html')


def channels(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'channels/index.html')


def channel(request, channel_id):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'channel/index.html', {'channel_id': channel_id})


def story(request, story_id):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'story/index.html', {'story_id': story_id})

def user(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'user/index.html')

def signup(request):
    # data = {
    #     'message': 'chat app index page'
    # }
    # return JsonResponse(data)
    # template = loader.get_template('chat/index.html')
    # return HttpResponse(template.render({}))
    return render(request, 'signup/index.html')

