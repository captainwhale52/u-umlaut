from backend.view import views
from django.conf.urls import include, url
from rest_framework import routers


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^photo/', views.photo, name='photo'),
    url(r'^composite/', views.composite, name='composite'),
    url(r'^dialogue/', views.dialogue, name='dialogue'),
    url(r'^channels/', views.channels, name='channels'),
    url(r'^channel/(?P<channel_id>\d+)/', views.channel, name='channel'),
    url(r'^user/', views.user, name='user'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^story/(?P<story_id>\d+)/', views.story, name='story'),
]
